//
//  VideoPlayer.swift
//  TestVid
//
//  Created by Mehdi Kitane on 02/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import AVFoundation


class VideoPlayer : NSObject{
    
    var avPlayer : AVPlayer!
    var wasPlaying = false
    var urlToPlay: URL?
    
    var delegate : VideoPlayerDelegate? = nil
    
    var thumbnail : UIImage? {
        get {
            do {
                let avAsset = AVAsset(url: urlToPlay!)
                let imageGenerator = AVAssetImageGenerator(asset: avAsset)
                imageGenerator.appliesPreferredTrackTransform = true
                let time = CMTimeMake(value: 0, timescale: 1)
                let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
                return UIImage(cgImage: imageRef)
            } catch {
                return nil
            }
        }
    }
    
    var duration : CMTime? {
        get {
            if avPlayer.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                return avPlayer.currentItem?.asset.duration
            }
            if avPlayer.currentItem?.status == AVPlayerItem.Status.unknown {
                return avPlayer.currentItem?.asset.duration
            }
            return nil
        }
    }
    
    var floatDuration : Float? {
        get {
            if let duration = duration {
                return Float(CMTimeGetSeconds(duration))
            }
            return nil
        }
    }
    
    var isPlaying : Bool {
        get {
            return avPlayer.rate != 0 && avPlayer.error == nil
        }
    }
    
    var currentTime : CMTime {
        get {
            return avPlayer.currentTime()
        }
    }
    
    init(urlToPlay : URL) {
        super.init()
        
        self.urlToPlay = urlToPlay
        
        let avAsset = AVAsset(url: urlToPlay)
        let avPlayerItem = AVPlayerItem(asset: avAsset)
        self.avPlayer = AVPlayer(playerItem: avPlayerItem)
        
        
        avPlayer.actionAtItemEnd = AVPlayer.ActionAtItemEnd.pause
        avPlayer.currentItem?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)
        
        
        //Updating Periodically the UI
        avPlayer.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            self.delegate?.shouldUpdateUI(self)
        }
        
        //Register when end
        let endTime : CMTime = self.duration!
        let values = [NSValue(time: endTime)]
        var tObs : AnyObject? = nil;
        tObs = avPlayer.addBoundaryTimeObserver(forTimes: values, queue: DispatchQueue.main, using: { () -> Void in
            if tObs != nil {
                //avPlayer.removeTimeObserver(tObs!)
                //tObs = nil
                self.avPlayer.seek(to: CMTime.zero)
                self.delegate?.videoEnded()
            }
        }) as AnyObject
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status" {
            if let _ = object as? AVPlayerItem {
                if let avPlayer = self.avPlayer {
                    if avPlayer.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                        self.delegate?.shouldUpdateUI(self)
                    }
                }
            }
        }
    }
    
    func pause(editWasPlaying : Bool) {
        if editWasPlaying {
            wasPlaying = false
        }
        avPlayer.pause()
    }
    
    func play() {
        wasPlaying = true
        avPlayer.play()
    }
    
    func seekToTime(_ value : Double) {
        let newTime = CMTimeMakeWithSeconds(value, preferredTimescale: (self.duration?.timescale)!);
        avPlayer.seek(to: newTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        self.delegate?.shouldUpdateUI(self)
    }
    
    func appendToView(_ videoHolderView : UIView) {
        let playerLayer = AVPlayerLayer(player: self.avPlayer)
        playerLayer.frame = videoHolderView.layer.bounds;
        playerLayer.videoGravity = GlobalConstants.Sizes.videoPlayerLayerSize
        playerLayer.needsDisplayOnBoundsChange = true
        videoHolderView.layer.addSublayer(playerLayer)
    }
    
}
