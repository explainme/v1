//
//  VideoMethods.swift
//  Explainme
//
//  Created by Mehdi Kitane on 03/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import AVFoundation

protocol CaptureObjectProtocol {
    func didFinishVideoRecordingToOutputFileAtURL(_ outputFileURL: URL!)
}
extension CaptureObject {
    func currentTime() -> CFTimeInterval {
        if let outputVideo = outputVideo {
            return CMTimeGetSeconds(outputVideo.recordedDuration)
        }
        return -1
    }
    
    func startRecording() {
        if let input = input {
            do {
                try input.device.lockForConfiguration()
                input.device.focusMode = .locked
                input.device.unlockForConfiguration()
            } catch {}
        }
        
        if let outputVideo = outputVideo {
            if !outputVideo.isRecording {
                //self.lockInterfaceRotation = true
                
                if UIDevice.current.isMultitaskingSupported {
                    //self.backgroundRecordId = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({})
                }
                
                let outputFilePath = Exporter.createOutputUrl("movie.mov")
                //let outputFilePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("movie.mov")
                outputVideo.startRecording(to: outputFilePath, recordingDelegate: self)
            }

        }
    }
    func stopRecording() {
        outputVideo!.stopRecording()

        if let input = input {
            do {
                try input.device.lockForConfiguration()
                input.device.focusMode = .continuousAutoFocus
                input.device.unlockForConfiguration()
            } catch {}
        }
    }
}
