//
//  Annotation.swift
//  TestVid
//
//  Created by Mehdi Kitane on 29/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import DeviceKit
import Surge

enum VidOrientation {
    case none, up, landscapeRight, portrait, landscapeLeft
}

let groupOfAllowedDevices: [Device] = [.iPhone6, .iPhone6Plus, .iPhone6s, .iPhone6sPlus, .iPhone7, .iPhone7Plus]

class Annotation {
    var time : TimeInterval
    var originalLayer : CALayer?
    var referenceSize : CGSize = CGSize.zero
    var sizeFactor  : CGFloat = 1

    
    init(time : TimeInterval) {
        self.time = time
        let device = Device()
        
        if device.isOneOf(groupOfAllowedDevices) {
            sizeFactor = 0.90
        }
    }
    
    func generateLayer(forSize size : CGSize, forOrientation orientation : VidOrientation, color : UIColor) -> CALayer {
        preconditionFailure("This method must be overridden")
    }

    func appendAnimationToLayer(_ layer : CALayer, atTime time : CFTimeInterval = 0, isExport : Bool = true) {
        let fadeInAndOut = CAKeyframeAnimation(keyPath: "opacity")
        fadeInAndOut.duration = GlobalConstants.Durations.annotationDuration;
        fadeInAndOut.autoreverses = false;
        
        fadeInAndOut.keyTimes = [NSNumber(value: 0.0 as Float),
            NSNumber(value: 0.3 as Float),
            NSNumber(value: 0.7 as Float),
            NSNumber(value: 1.0 as Float)]
        
        fadeInAndOut.values = [NSNumber(value: 0.0 as Float),
            NSNumber(value: 1.0 as Float),
            NSNumber(value: 1.0 as Float),
            NSNumber(value: 0.0 as Float)]
        
        if isExport == true {
            fadeInAndOut.beginTime = time;
        } else {
            fadeInAndOut.beginTime = CACurrentMediaTime() + time;
        }
        fadeInAndOut.isRemovedOnCompletion = false;
        fadeInAndOut.fillMode = CAMediaTimingFillMode.both;
        
        //Append AnimationToLayer
        layer.add(fadeInAndOut, forKey: nil)
    }
    
    func rotatePointToPortrait(_ cPos: CGPoint, size: CGSize) -> CGPoint {
        //http://morpheo.inrialpes.fr/people/Boyer/Teaching/RICM/c3.pdf
        //https://developer.apple.com/library/content/documentation/General/Conceptual/Devpedia-CocoaApp/CoordinateSystem.html
        
        //M1 = translation (tx, ty)
        let tx : Double = 0
        let ty : Double = Double(size.height)
        let M1 : Matrix<Double> = Matrix([
            [1, 0, tx],
            [0, 1, ty],
            [0, 0, 1]
        ])
        //M2 = Symetrie axe x
        let M2 : Matrix<Double> = Matrix([
            [1, 0, 0],
            [0, -1, 0],
            [0, 0, 1]
        ])
        //M3 = Changement dechelle (sx, sy)
        let sx : Double = Double(size.width / referenceSize.width)
        let sy : Double = Double(size.height / referenceSize.height)
        let M3 : Matrix<Double> = Matrix([
            [sx,0,  0],
            [0, sy, 0],
            [0, 0,  1]
        ])

        let V1 : Matrix<Double> = Matrix([
            [Double(cPos.x)],
            [Double(cPos.y)],
            [1]
        ])
        let V2 = M1*M2*M3*V1

        let rx = CGFloat(V2[column: 0][0])
        let ry = CGFloat(V2[column: 0][1])
        return CGPoint(x: rx, y: ry)
    }
    
    func rotatePointToRight(_ cPos: CGPoint, size: CGSize) -> CGPoint {
        //M1 = Rotation theta = PI/2
        let theta = 1.0 * Double.pi / 2
        let M1 : Matrix<Double> = Matrix([
            [cos(theta), -sin(theta), 0],
            [sin(theta), cos(theta), 0],
            [0, 0, 1]
        ])
        
        //M2 = Symetrie axe x
        let M2 : Matrix<Double> = Matrix([
            [1, 0, 0],
            [0, -1, 0],
            [0, 0, 1]
        ])
        
        //M3 = Changement dechelle (sx, sy)
        let sx : Double = Double(size.width / referenceSize.height)
        let sy : Double = Double(size.height / referenceSize.width)
        let M3 : Matrix<Double> = Matrix([
            [sx,0,  0],
            [0, sy, 0],
            [0, 0,  1]
        ])
        
        let V1 : Matrix<Double> = Matrix([
            [Double(cPos.x)],
            [Double(cPos.y)],
            [1]
        ])
        let V2 = M1*M2*M3*V1

        let rx = CGFloat(V2[column: 0][0])
        let ry = CGFloat(V2[column: 0][1])
        return CGPoint(x: rx, y: ry)
    }

    func rotatePointToLeft(_ cPos: CGPoint, size: CGSize) -> CGPoint {
        //M1 = Rotation theta = PI/2
        let theta = 1.0 * Double.pi / 2
        let M1 : Matrix<Double> = Matrix([
            [cos(theta), -sin(theta), 0],
            [sin(theta), cos(theta), 0],
            [0, 0, 1]
        ])
        
        //M2 = Symetrie axe x
        let M2 : Matrix<Double> = Matrix([
            [1, 0, 0],
            [0, -1, 0],
            [0, 0, 1]
        ])
        
        //M3 = Changement dechelle (sx, sy)
        let sx : Double = Double(size.width / referenceSize.height)
        let sy : Double = Double(size.height / referenceSize.width)
        let M3 : Matrix<Double> = Matrix([
            [sx,0,  0],
            [0, sy, 0],
            [0, 0,  1]
        ])
        
        //M4 - Translation tx ty
        let tx : Double = 1.0 * Double(referenceSize.width)
        let ty : Double = 1.0 * Double(referenceSize.height)
        let M4 : Matrix<Double> = Matrix([
            [1, 0, tx],
            [0, 1, ty],
            [0, 0, 1]
        ])
        
        //M5 = Symetrie axe y
        let M5 : Matrix<Double> = Matrix([
            [-1, 0, 0],
            [0, 1, 0],
            [0, 0, 1]
        ])
        
        //M6 = Symetrie axe x
        let M6 : Matrix<Double> = Matrix([
            [1, 0, 0],
            [0, -1, 0],
            [0, 0, 1]
        ])
        
        let V1 : Matrix<Double> = Matrix([
            [Double(cPos.x)],
            [Double(cPos.y)],
            [1]
        ])
        let V2 = M1*M2*M3*M4*M5*M6*V1

        let rx = CGFloat(V2[column: 0][0])
        let ry = CGFloat(V2[column: 0][1])
        return CGPoint(x: rx, y: ry)
    }

}
