//
//  VideoPlayerDelegate.swift
//  TestVid
//
//  Created by Mehdi Kitane on 02/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation

protocol VideoPlayerDelegate {
    func shouldUpdateUI(_ videoPlayer : VideoPlayer)
    func videoEnded()
}
