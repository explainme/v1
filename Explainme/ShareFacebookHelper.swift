//
//  ShareFacebookHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 13/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import FBSDKShareKit
import AssetsLibrary

class ShareFacebookHelper : NSObject, FBSDKSharingDelegate {
    func shareToFacebook(_ url : URL, viewController: UIViewController, assetURL : inout URL?) {
        let weak = self
        
        if let newURL = assetURL {
            print("alreadySaved will not save twice")
            self.saveHelper(newURL, viewController: viewController, weak: weak)
        } else {
            LibraryExporter.saveVideoToCameraRoll(url, toAlbum: "Explain.me") { (success, assetURLFrom) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    if let newURL = assetURLFrom {
                        if let viewController = viewController as? ShareViewController {
                            viewController.assetURL = newURL
                        }
                        self.saveHelper(newURL, viewController: viewController, weak: weak)
                    }
                })
            }
        }

    }
    
    func saveHelper(_ newUrl : URL, viewController : UIViewController, weak : FBSDKSharingDelegate!) {
        
        //print(newUrl)
        
        let video = FBSDKShareVideo()
        video.videoURL = newUrl
        
        let content = FBSDKShareVideoContent()
        content.video = video
        
        
        //Documentation :
        //https://developers.facebook.com/docs/sharing/ios
        
        FBSDKShareDialog.show(from: viewController, with: content, delegate: weak)
        
        
        /*
        let dialog = FBSDKShareDialog()
        dialog.fromViewController = viewController
        dialog.mode = FBSDKShareDialogMode.ShareSheet
        dialog.shareContent = content
        dialog.delegate = weak
        dialog.show()
        */
        //FBSDKMessageDialog.showWithContent(content, delegate: weak)
        //FBSDKShareAPI.shareWithContent(content, delegate: weak)
    }
    
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        
    }
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        if let error = error {
            let message = "Facebook is not installed in your iphone, please install it to share via Facebook"
            UIAlertView(title: "Facebook", message: message, delegate: nil, cancelButtonTitle: "Cancel").show()
            print(error)
        }
    }
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
}
