//
//  CaptureObject.swift
//  TestVid
//
//  Created by Mehdi Kitane on 02/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import AVFoundation

var SessionRunningAndDeviceAuthorizedContext = "SessionRunningAndDeviceAuthorizedContext"
var CapturingStillImageContext = "CapturingStillImageContext"
var RecordingContext = "RecordingContext"


class CaptureObject : NSObject, AVCaptureFileOutputRecordingDelegate {
    //It's a videoMethod method.
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        shouldLockInterface = false
        self.delegate?.didFinishVideoRecordingToOutputFileAtURL(outputFileURL)
        self.delegate = nil
    }
    
    var session : AVCaptureSession!
    var previewLayer : AVCaptureVideoPreviewLayer!
    var input : AVCaptureDeviceInput?
    
    var deviceAuthorized: Bool  = false
    var shouldLockInterface : Bool = false
    
    
    //Image
    var outputImage : AVCaptureStillImageOutput?
    //Video 
    var outputVideo : AVCaptureMovieFileOutput?
    //var completionHandlerVideo : ((url : NSURL) -> Void) = {_ in }
    var delegate : CaptureObjectProtocol? = nil
    
    override init() {
        super.init()
        session = AVCaptureSession()
        session.sessionPreset = AVCaptureSession.Preset.iFrame1280x720
        
        let videoDevice : AVCaptureDevice = AVCaptureDevice.default(for: .video)!
        do {
            try videoDevice.lockForConfiguration()
            videoDevice.focusMode = .continuousAutoFocus
            videoDevice.unlockForConfiguration()
        } catch {}

        do {
            try input = AVCaptureDeviceInput(device: videoDevice)
            if let input = input {
                if( session.canAddInput(input) ) {
                    session.addInput(input)
                }
            }
            
            let audioDevice: AVCaptureDevice? = AVCaptureDevice.devices(for: AVMediaType.audio).first
            if let audioDevice = audioDevice {
                let audioDeviceInput: AVCaptureDeviceInput? = try AVCaptureDeviceInput(device: audioDevice)
                if let audioDeviceInput = audioDeviceInput {
                    if session.canAddInput(audioDeviceInput){
                        session.addInput(audioDeviceInput)
                    }
                }
            }
        } catch {
            
        }
        
        
        outputImage = AVCaptureStillImageOutput()
        //outputImage!.automaticallyEnablesStillImageStabilizationWhenAvailable = true
        outputImage!.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
        outputVideo = AVCaptureMovieFileOutput()
        outputVideo?.maxRecordedDuration = CMTimeMake(value: Int64(GlobalConstants.RecordingTimes.videoRecordingTime), timescale: 1)
        
        let connectionIm: AVCaptureConnection? = outputImage!.connection(with: AVMediaType.video)
        connectionIm?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto

        let connectionVid: AVCaptureConnection? = outputVideo!.connection(with: AVMediaType.video)
        connectionVid?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
        
        if self.session.canAddOutput(self.outputImage!) {
            self.session.addOutput(self.outputImage!)
        }
        if self.session.canAddOutput(self.outputVideo!) {
            self.session.addOutput(self.outputVideo!)
        }
        

        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.videoGravity = GlobalConstants.Sizes.previewLayerCaptureRunningSize        
    }
    
    func append(inView view: UIView, atlayerIndex layerLevel: Int) {
        //PreviewLayer
        /*dispatch_async(dispatch_get_main_queue(), {
            //self.previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait //Maybe remove
        })*/
        if let previewLayer = previewLayer {
            if previewLayer.superlayer != view.layer {
                print("will append")
                previewLayer.removeFromSuperlayer()
                previewLayer.frame = view.bounds
                view.layer.insertSublayer(previewLayer, at: UInt32(layerLevel))
            }
        }
    }
    

    static let cameraQueue = DispatchQueue(label: "com.explain.me.Queue", attributes: []);
    /*func configureForImage(completion : (Void) -> Void) {

        dispatch_async(cameraQueue) { () -> Void in
            self.session.beginConfiguration()
            self.session.removeOutput(self.outputVideo)
            
            if self.session.canAddOutput(self.outputImage) {
                self.session.addOutput(self.outputImage)
            }
            self.session.commitConfiguration()
            dispatch_async(dispatch_get_main_queue(), {
                completion()
            })
        }
    }
    
    
    func configureForVideo(completion : (Void) -> Void) {
        dispatch_async(cameraQueue) { () -> Void in
            self.session.beginConfiguration()
            self.session.removeOutput(self.outputImage)
            
            if self.session.canAddOutput(self.outputVideo) {
                self.session.addOutput(self.outputVideo)
            }
            self.session.commitConfiguration()
            dispatch_async(dispatch_get_main_queue(), {
                completion()
            })
        }
        /*dispatch_async(dispatch_get_main_queue(), {
        //Maybe to add
        let orientation: AVCaptureVideoOrientation =  AVCaptureVideoOrientation(rawValue: self.interfaceOrientation.rawValue)!
        (self.recordView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation = orientation
        })*/
    }*/
    
    
    func startRunning() {
        session.startRunning()
    }
    func stopRunning() {
        session.stopRunning()
    }

    
    // MARK : - Handling Orientation
    func willRotateToInterfaceOrientation(_ toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        previewLayer.connection!.videoOrientation = AVCaptureVideoOrientation(rawValue: toInterfaceOrientation.rawValue)!
        setOrientationForImage(toInterfaceOrientation)
        setOrientationForVideo(toInterfaceOrientation)
    }
    
    func didRotateToInterfaceOrientation(_ fromInterfaceOrientation: UIInterfaceOrientation, withView view : UIView) {
        previewLayer.frame = view.bounds
    }
    
    func shouldAutorotate() -> Bool {
        return !shouldLockInterface
    }
    
    func setOrientationForVideo(_ interfaceOrientation: UIInterfaceOrientation) {
        if let outputVideo = outputVideo {
            var videoConnection : AVCaptureConnection?
            for connection : AVCaptureConnection  in outputVideo.connections  {
                for port : AVCaptureInput.Port in connection.inputPorts {
                    if port.mediaType == AVMediaType.video {
                        videoConnection = connection
                        break
                    }
                }
                if (videoConnection != nil) {
                    break
                }
            }
            
            videoConnection?.videoOrientation = AVCaptureVideoOrientation(rawValue: interfaceOrientation.rawValue)!
        }
    }
    func setOrientationForImage(_ interfaceOrientation: UIInterfaceOrientation) {
        if let outputImage = outputImage {
            var videoConnection : AVCaptureConnection?
            for connection : AVCaptureConnection  in outputImage.connections  {
                for port : AVCaptureInput.Port in connection.inputPorts {
                    if port.mediaType == AVMediaType.video {
                        videoConnection = connection
                        break
                    }
                }
                if (videoConnection != nil) {
                    break
                }
            }
            
            videoConnection?.videoOrientation = AVCaptureVideoOrientation(rawValue: interfaceOrientation.rawValue)!
        }
    }
}



/*
func viewWillAppear() {
    dispatch_async(self.sessionQueue, {
        self.addObserver(self, forKeyPath: "sessionRunningAndDeviceAuthorized", options: [.Old , .New] , context: &SessionRunningAndDeviceAuthorizedContext)
        self.addObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", options:[.Old , .New], context: &CapturingStillImageContext)
        self.addObserver(self, forKeyPath: "movieFileOutput.recording", options: [.Old , .New], context: &RecordingContext)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "subjectAreaDidChange:", name: AVCaptureDeviceSubjectAreaDidChangeNotification, object: self.videoDeviceInput?.device)
        
        
        weak var weakSelf = self
        
        self.runtimeErrorHandlingObserver = NSNotificationCenter.defaultCenter().addObserverForName(AVCaptureSessionRuntimeErrorNotification, object: self.session, queue: nil, usingBlock: {
            (note: NSNotification?) in
            let strongSelf: RecordViewController = weakSelf!
            dispatch_async(strongSelf.sessionQueue, {
                if let sess = strongSelf.session {
                    sess.startRunning()
                }
            })
        })
    })
}
func viewWillDisappear() {
    dispatch_async(self.sessionQueue, {
        
        if let sess = self.session{
            sess.stopRunning()
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: AVCaptureDeviceSubjectAreaDidChangeNotification, object: self.videoDeviceInput?.device)
            NSNotificationCenter.defaultCenter().removeObserver(self.runtimeErrorHandlingObserver!)
            
            self.removeObserver(self, forKeyPath: "sessionRunningAndDeviceAuthorized", context: &SessionRunningAndDeviceAuthorizedContext)
            
            self.removeObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", context: &CapturingStillImageContext)
            self.removeObserver(self, forKeyPath: "movieFileOutput.recording", context: &RecordingContext)
            
        }
        
    })
    
}

override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {



if context == &CapturingStillImageContext{
let isCapturingStillImage: Bool = change![NSKeyValueChangeNewKey]!.boolValue
if isCapturingStillImage {
self.runStillImageCaptureAnimation()
}

} else if context  == &RecordingContext {
let isRecording: Bool = change![NSKeyValueChangeNewKey]!.boolValue

dispatch_async(dispatch_get_main_queue(), {

if isRecording {
/*self.recordButton.titleLabel!.text = "Stop"
self.recordButton.enabled = true
self.cameraButton.enabled = false*/

} else {

/*self.recordButton.titleLabel!.text = "Record"
self.recordButton.enabled = true
self.cameraButton.enabled = true*/

}


})


}

else{
return super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
}

}



// MARK: Selector
func subjectAreaDidChange(notification: NSNotification){
let devicePoint: CGPoint = CGPoint(x: 0.5, y: 0.5)
self.focusWithMode(AVCaptureFocusMode.ContinuousAutoFocus, exposureMode: AVCaptureExposureMode.ContinuousAutoExposure, point: devicePoint, monitorSubjectAreaChange: false)
}




*/
