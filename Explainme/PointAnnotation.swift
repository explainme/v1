//
//  PointAnnotation.swift
//  TestVid
//
//  Created by Mehdi Kitane on 29/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import Surge

class PointAnnotation : Annotation {
    var position : CGPoint

    init(x : CGFloat, y : CGFloat, time : TimeInterval) {
        self.position = CGPoint(x: x, y: y)
        super.init(time: time)
    }
    
    //Configs
    let radius : CGFloat = 10
    let borderWidth : CGFloat = 2
    
    override func generateLayer(forSize size : CGSize, forOrientation orientation : VidOrientation, color : UIColor) -> CALayer {
        if referenceSize == CGSize.zero {
            referenceSize = size
        }

        var isNotLandscape : CGFloat = 1.0
        if (orientation == .landscapeLeft || orientation == .landscapeRight) {
            isNotLandscape = size.height / size.width
        }

        let factorX : CGFloat = size.width / referenceSize.width
        let factorY : CGFloat = size.height / referenceSize.height

        //position is the position of the center
        var cPos : CGPoint = CGPoint(x: position.x, y: position.y)
        let cRad : CGFloat = factorX * radius * isNotLandscape * sizeFactor
        let cBord : CGFloat = factorX * borderWidth * isNotLandscape * sizeFactor

        if (orientation == .none) {
            cPos = CGPoint(x: factorX * cPos.x, y: factorY * cPos.y)
        }

        if(orientation == .portrait) {
            cPos = rotatePointToPortrait(cPos, size: size)
        }
        
        if(orientation == .landscapeRight) {
            cPos = rotatePointToRight(cPos, size: size)
        }
        
        if(orientation == .landscapeLeft) {
            cPos = rotatePointToLeft(cPos, size: size)
        }

        let overlayLayer = CAShapeLayer()
        
        overlayLayer.frame = CGRect(x: cPos.x - cRad, y: cPos.y - cRad, width: 2*cRad, height: 2*cRad)
        overlayLayer.cornerRadius = cRad
        overlayLayer.borderWidth = cBord

        
        overlayLayer.backgroundColor = color.cgColor
        overlayLayer.masksToBounds = false
        overlayLayer.borderColor = UIColor.white.cgColor
        
        return overlayLayer
    }
}
