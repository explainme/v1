//
//  AudioCapture.swift
//  TestVid
//
//  Created by Mehdi Kitane on 01/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import AVFoundation

protocol AudioCaptureProtocol {
    func didFinishAudioRecordingToOutputFileAtURL(_ outputFileURL: URL!)
}

class AudioCapture : NSObject, AVCaptureAudioDataOutputSampleBufferDelegate{
    var session: AVCaptureSession!
    var audioOutput: AVCaptureAudioDataOutput?
    var writer : AVAssetWriter!
    var audioWriterInput : AVAssetWriterInput!
    var isRecording = false
    
    var currentDuration : CMTime = CMTime.zero
    var audioDelegate : AudioCaptureProtocol? = nil 
    var timer : Timer? = nil
    var shouldWriteToFile = false
    
    override init() {
        super.init()

        session = AVCaptureSession()
        do {
            let audioDevice: AVCaptureDevice = AVCaptureDevice.default(.builtInMicrophone, for: AVMediaType.audio, position: .unspecified)!
            var audioDeviceInput: AVCaptureDeviceInput?
            audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
            
            if let audioDeviceInput = audioDeviceInput {
                if session.canAddInput(audioDeviceInput){
                    session.addInput(audioDeviceInput)
                }
            }
            
            audioOutput = AVCaptureAudioDataOutput()
            if let audioOutput = audioOutput {
                if self.session.canAddOutput(audioOutput){
                    self.session.addOutput(audioOutput)
                }
            }
            
            let connection: AVCaptureConnection? = audioOutput!.connection(with: AVMediaType.video)
            if ((connection?.isVideoStabilizationSupported) != nil) {
                connection!.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
            }

            //try AVAudioSession.sharedInstance().setInputGain(0.1)

            let queue = DispatchQueue(label: "MyQueue", attributes: []);
            audioOutput?.setSampleBufferDelegate(self, queue: queue)
        } catch let error as NSError {
            print(error)
        } catch {
            fatalError()
        }
    }
    
    func makeWriter() {
        let exportURL = Exporter.createOutputUrl("capture.m4a")        
        
        /*if ([[NSFileManager defaultManager] fileExistsAtPath:exportURL.path])
        {
        [[NSFileManager defaultManager] removeItemAtPath:exportURL.path error:nil];
        }*/
        
        /*AVURLAsset *soundTrackAsset = [[AVURLAsset alloc]initWithURL:trackUrl options:nil];
        AVMutableAudioMixInputParameters *audioInputParams = [AVMutableAudioMixInputParameters audioMixInputParameters];
        
        [audioInputParams setVolume:0.5 atTime:kCMTimeZero];
        [audioInputParams setTrackID:[[[soundTrackAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]  trackID]];
        audioMix = [AVMutableAudioMix audioMix];
        audioMix.inputParameters = [NSArray arrayWithObject:audioInputParams];*/

        
        do {
            var acl : AudioChannelLayout = AudioChannelLayout();
            acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
            
            let audioOutputSettings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100,
                AVEncoderBitRateKey : 64000,
                AVNumberOfChannelsKey: 1 as NSNumber
            ] as [String : Any]
            
            writer = try AVAssetWriter(outputURL: exportURL, fileType: AVFileType.m4a)
            audioWriterInput  = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: audioOutputSettings)
            writer.add(audioWriterInput)
        } catch {
            
        }
    }
    
    
    func currentTime() -> CFTimeInterval {
        return CMTimeGetSeconds(currentDuration)
    }
    
    func startAudio() {
        if shouldWriteToFile {
            timer = Timer.scheduledTimer(timeInterval: GlobalConstants.RecordingTimes.pictureRecordingTime + 1, target: self, selector: #selector(AudioCapture.stopAudio), userInfo: nil, repeats: false)
        }
        if !isRecording {
            makeWriter()
            session.startRunning()
            isRecording = true
        }
    }
    
    @objc func stopAudio() {
        //audioWriterInput.markAsFinished()
        //writer.endSessionAtSourceTime(CMTimeMake(0, 0))
        timer?.invalidate()
        timer = nil
        if isRecording {
            isRecording = false
            session.stopRunning()
            if shouldWriteToFile {
                writer.finishWriting { () -> Void in
                    self.audioDelegate?.didFinishAudioRecordingToOutputFileAtURL(self.writer.outputURL)
                    self.audioDelegate = nil
                }
            }
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if !CMSampleBufferDataIsReady(sampleBuffer) {
            print("sample buffer is not ready. Skipping sample" )
            return
        }
        
        if writer.status == AVAssetWriter.Status.failed {
            session.stopRunning()
            return
        }
        if isRecording && output == audioOutput {
            let lastSample = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            currentDuration = CMTimeAdd(currentDuration, CMSampleBufferGetOutputDuration(sampleBuffer))
            if writer.status != AVAssetWriter.Status.writing {
                writer.startWriting()
                writer.startSession(atSourceTime: lastSample)
            }
            
            if writer.status.rawValue > AVAssetWriter.Status.writing.rawValue {
                print("Warning : writer status is : \(writer.status.hashValue)")
            }
            
            if !audioWriterInput.append(sampleBuffer) {
                print("Unable to write to audio input")
            }
            
        }
    }
    
    static func checkDeviceAuthorizationStatus(_ completion : @escaping (_ granted : Bool, _ firstTime: Bool) -> Void ){
        let mediaType:String = AVMediaType.audio.rawValue;
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType(rawValue: mediaType))
        if(authStatus == .authorized) {
            completion(true, false)
        } else if(authStatus == .denied){
            // denied
            completion(false, false)
        } else if(authStatus == .restricted){
            // restricted, normally won't happen
            completion(false, false)
        } else if(authStatus == .notDetermined){
            // not determined?!
            AVCaptureDevice.requestAccess(for: AVMediaType(rawValue: mediaType), completionHandler: { (granted: Bool) in
                //self.deviceAuthorized = granted;
                completion(granted, true)
            })
        } else {
            // impossible, unknown authorization status
            completion(false, false)
        }
    }
}
