//
//  ProgressButtonLayer.swift
//  Explainme
//
//  Created by Mehdi Kitane on 28/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import UIKit

class ProgressButtonLayer: UIView {

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        if let view = self.superview as? ProgressButton {
            let color = UIColor.white.cgColor
            let lineWidth : CGFloat = 2.0
            let x = frame.width/2
            let y = frame.height/2
            let radius = frame.width/2 - 5
            
            
            let context = UIGraphicsGetCurrentContext()
            context?.setLineWidth(lineWidth)
            context?.setStrokeColor(color)
            if view.progression == 1.0 {
                let startAngle : CGFloat = 0.0
                let endAngle = 2.0*CGFloat(Double.pi)-0.001
                
                let point = CGPoint(x: x, y: y)
                context?.addArc(center: point, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
            } else {
                let startAngle : CGFloat = 3 * CGFloat(Double.pi)/2.0
                let endAngle : CGFloat = view.progression * 2 * CGFloat(Double.pi) + startAngle
                
                let point = CGPoint(x: x, y: y)
                context?.addArc(center: point, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
            }
            context?.strokePath()
        }
    }
    

}
