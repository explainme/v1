//
//  ShareHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 17/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class ShareInstagramHelper : NSObject, UIDocumentInteractionControllerDelegate {
    
    let documentationInteractionController = UIDocumentInteractionController()
    
    func sendFromInstagram(_ urlToSave : URL?, viewController : UIViewController) {
        //if let filePath =  NSBundle.mainBundle().pathForResource("test", ofType: "mov") {
        //let urlToSav = NSURL(fileURLWithPath: 
        

        if let url = urlToSave {
            let window = UIApplication.shared.windows.last
            let hud = MBProgressHUD.showAdded(to: window!, animated: true)
            hud.label.text = "Exporting to Instagram"
            LibraryExporter.saveVideoToCameraRoll(url, toAlbum: "Explain.me") { (success, assetURL) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    hud.hide(animated: true)
                    
                    if let assetURL = assetURL {
                        let assetPath = assetURL.absoluteString.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)!
                        let instagramString = "instagram://library?AssetPath=" + assetPath
                        
                        let instaURL = URL(string: instagramString)
                        UIApplication.shared.openURL(instaURL!)
                    }
                })
            }

        }
    }
    
    
    func canOpenWhatsapp() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: "instagram://location?id=1")!)
    }
}

