//
//  ViewController.swift
//  Test
//
//  Created by Mehdi Kitane on 04/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import UIKit
import AssetsLibrary
import MBProgressHUD
import PhotosUI

class ShareViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    
    @IBOutlet var globalView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var socialCollectionView: UICollectionView!
    @IBOutlet weak var privateCollectionView: UICollectionView!
    @IBOutlet weak var videoTitle: UITextField!
    @IBOutlet weak var socialMediaTitle: UILabel!
    @IBOutlet weak var privateMediaTitle: UILabel!
    //@IBOutlet weak var explainMeMoreButton: UIButton!
    //@IBOutlet weak var saveToGalleryButton: UIButton!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var Veil: UIView!
    
    var movieFileOutput: URL?
    var thumbnail : UIImage?
    var assetURL : URL? = nil
    
    
    //FirstMethod for layout
    @IBOutlet weak var equalHeightSocialPrivate: NSLayoutConstraint!
    @IBOutlet weak var equalHeightPrivateJustSave: NSLayoutConstraint!
    @IBOutlet weak var equalHeightScrollViewContentView: NSLayoutConstraint!
    //SecondMethod
    @IBOutlet weak var bottomSaveToGallery: NSLayoutConstraint!
    @IBOutlet weak var heightJustSave: NSLayoutConstraint!
    @IBOutlet weak var heightSocial: NSLayoutConstraint!
    @IBOutlet weak var heightPrivate: NSLayoutConstraint!
    
    //private let socialMediaNames = ["Youtube", "Facebook","Twitter", "Instagram", "Pinterest"]
    fileprivate let socialMediaNames = ["Youtube", "Facebook", "Twitter", "Instagram"]
    
    //private let privateMediaNames = ["Email", "Whatsapp", "Slack", "Box", "Dropbox", "GoogleDrive", "iCloud", "Onedrive"]
    fileprivate let privateMediaNames = [ "SaveImage", "Email", "FacebookMessenger", "Whatsapp", "More"]
    
    
    //ShareHelpers :
    let shareMail = ShareMailHelper()
    let shareWhatsapp = ShareWhatsappHelper()
    let shareSlack = ShareSlackHelper()
    let shareYoutube = ShareYoutubeHelper()
    let shareFacebook = ShareFacebookHelper()
    let shareTwitter = ShareTwitterHelper()
    let shareFacebookMessenger = ShareFacebookMessengerHelper()
    let shareInstagram = ShareInstagramHelper()

    var lastOrientation : UIInterfaceOrientation = UIInterfaceOrientation.unknown
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        if (GlobalConstants.mediaChoice == .video) {
            globalView.backgroundColor = GlobalConstants.Colors.pink
            headerView.backgroundColor = GlobalConstants.Colors.pink
            socialMediaTitle.textColor = GlobalConstants.Colors.pink
            privateMediaTitle.textColor = GlobalConstants.Colors.pink
            //explainMeMoreButton.backgroundColor = GlobalConstants.Colors.pink
            //saveToGalleryButton.setTitleColor(GlobalConstants.Colors.pink, for: UIControlState())
            logoView.image = UIImage(named: "logo_red")
        }
        
        scrollView.delegate = self
        scrollView.alwaysBounceVertical = true
        
        socialCollectionView.delegate = self
        socialCollectionView.dataSource = self
        socialCollectionView.showsHorizontalScrollIndicator = false
        socialCollectionView.alwaysBounceHorizontal = true
        
        privateCollectionView.delegate = self
        privateCollectionView.dataSource = self
        privateCollectionView.showsHorizontalScrollIndicator = false
        privateCollectionView.alwaysBounceHorizontal = true
        
        if let thumbnail = thumbnail {
            var newImageOrientation = thumbnail.imageOrientation
            if (GlobalConstants.mediaChoice == .photo) {
                if lastOrientation == .landscapeRight { newImageOrientation = UIImage.Orientation.down }
                if lastOrientation == .landscapeLeft { newImageOrientation = UIImage.Orientation.up }
            }
            if (GlobalConstants.mediaChoice == .video) {
                if lastOrientation == .landscapeRight { newImageOrientation = UIImage.Orientation.right }
                if lastOrientation == .landscapeLeft { newImageOrientation = UIImage.Orientation.left }
            }
            
            self.thumbnail =  UIImage(cgImage: rotateImage(thumbnail.cgImage, orientation: newImageOrientation)!)
        }
        
        headerImageView.image = self.thumbnail
        
        //videoTitle.becomeFirstResponder()
        //videoTitle.canBecomeFirstResponder()
        let gestureForTap = UITapGestureRecognizer(target: self, action: #selector(ShareViewController.clickedVeil))
        Veil.addGestureRecognizer(gestureForTap)
        videoTitle.delegate = self
        let color = UIColor.lightText
        videoTitle.attributedPlaceholder = NSAttributedString(string: "Video Title", attributes: [NSAttributedString.Key.foregroundColor : color])
        
        //let urlpath = NSBundle.mainBundle().pathForResource("small-cute-kitten", ofType: "mp4")
        //movieFileOutput = NSURL.fileURLWithPath(urlpath!)
        
        if let movieFileOutput = ExporterSingleton.sharedExporter.movieFileOutput {
            self.movieFileOutput = movieFileOutput as URL
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(ShareViewController.didFinishExporting), name: NSNotification.Name(rawValue: "finishedExporting"), object: nil)
        }
    }
    
    @objc func didFinishExporting() {
        if let movieFileOutput = ExporterSingleton.sharedExporter.movieFileOutput {
            self.movieFileOutput = movieFileOutput as URL
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //For iphone 4s and lower
        if UIScreen.main.nativeBounds.height == 960 {
            equalHeightSocialPrivate.isActive = false
            equalHeightPrivateJustSave.isActive = false
            equalHeightScrollViewContentView.isActive = false
            bottomSaveToGallery.isActive = true
            heightJustSave.isActive = true
            heightSocial.isActive = true
            heightPrivate.isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        
        if(offset < 0) {
            //pulldown
            let headerScaleFactor:CGFloat = -(offset) / headerView.bounds.height
            let headerSizevariation = ((headerView.bounds.height * (1.0 + headerScaleFactor)) - headerView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            headerView.layer.transform = headerTransform
            
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        headerView.layer.transform = CATransform3DIdentity
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == socialCollectionView) {
            return socialMediaNames.count;
        } else {
            return privateMediaNames.count;
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareItem", for: indexPath)
        
        if(collectionView == socialCollectionView) {
            cell.backgroundView = UIImageView(image: UIImage(named: socialMediaNames[indexPath.row]))
            //cell.backgroundColor = socialMediaNames[indexPath.row]
        } else {
            cell.backgroundView = UIImageView(image: UIImage(named: privateMediaNames[indexPath.row]))
            //cell.backgroundColor = privateMediaNames[indexPath.row]
        }
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let movieFileOutput = movieFileOutput {
            var title : String?
            if videoTitle.text != nil  && videoTitle.text?.characters.count != 0 {
                title = videoTitle.text
            } else {
                title = "Shared via Explainᴹᴱ"
            }
            
            if(collectionView == socialCollectionView) {
                let cell = socialMediaNames[indexPath.row]
                if cell == "Youtube" {
                    shareYoutube.shareToYoutube(title!, description: "", url: movieFileOutput)
                    return
                    //let message = "Youtube is not implemented yet, stay tuned!"
                    //UIAlertView(title: "Not Implemented", message: message, delegate: nil, cancelButtonTitle: "Cancel").show()
                }
                if cell == "Twitter" {
                    shareTwitter.share(title!, url: movieFileOutput)
                    return
                    //let message = "Twitter is not implemented yet, stay tuned!"
                    //UIAlertView(title: "Not Implemented", message: message, delegate: nil, cancelButtonTitle: "Cancel").show()
                }
                if cell == "Facebook" {
                    shareFacebook.shareToFacebook(movieFileOutput, viewController: self, assetURL: &assetURL)
                    return
                    //let message = "Facebook is not implemented yet, stay tuned!"
                    //UIAlertView(title: "Not Implemented", message: message, delegate: nil, cancelButtonTitle: "Cancel").show()
                }
                if cell == "Instagram" {
                    shareInstagram.sendFromInstagram(self.movieFileOutput, viewController: self)
                    return
                }
                if cell == "Pinterest" {
                    let message = "Pinterest is not implemented yet, stay tuned!"
                    UIAlertView(title: "Pinterest", message: message, delegate: nil, cancelButtonTitle: "Cancel").show()
                    return
                }
                
                let message = "This feature is not implemented yet, stay tuned!"
                UIAlertView(title: "Not Implemented", message: message, delegate: nil, cancelButtonTitle: "Cancel").show()
            } else {
                let cell = privateMediaNames[indexPath.row]
                if cell == "SaveImage" {
                    let window = UIApplication.shared.windows.last!
                    let hud = MBProgressHUD.showAdded(to: window, animated: true)
                    hud.label.text = "Saving to camera roll"
                    LibraryExporter.saveVideoToCameraRoll(movieFileOutput, toAlbum: "Explainᴹᴱ") { (success, assetURL) -> Void in
                        DispatchQueue.main.async(execute: { () -> Void in
                            hud.hide(animated: true)
                        })
                    }
                    return
                }
                if cell == "Email" {
                    shareMail.saveToEmail(title!, messageBody: "Here's my Explainᴹᴱ!", urlToSave: self.movieFileOutput, viewController: self)
                    return
                }
                if cell == "FacebookMessenger" {
                    shareFacebookMessenger.saveToMessenger(self.movieFileOutput, viewController: self)
                    return
                }
                if cell == "Whatsapp" {
                    shareWhatsapp.sendFromWhatsapp(self.movieFileOutput, viewController: self)
                    return
                }
                if cell == "Slack" {
                    shareSlack.sendFromSlack(self.movieFileOutput, viewController: self)
                    return
                }
                if cell == "More" {
                    shareSlack.sendFromSlack(self.movieFileOutput, viewController: self)
                    return
                }
                let message = "This feature is not implemented yet, stay tuned!"
                UIAlertView(title: "Not Implemented", message: message, delegate: nil, cancelButtonTitle: "Cancel").show()
            }
            
        } else {
            UIAlertView(title: "Sharing", message: "Please wait few seconds while we prepare your Explainᴹᴱ", delegate: nil, cancelButtonTitle: "Ok").show()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!,
                        sizeForItemAtIndexPath indexPath: IndexPath!) -> CGSize {
        return CGSize(width: 58, height: 58)
    }
    
    
    
    @IBAction func clickLogoTopRight(_ sender: AnyObject) {
        self.clickedJustSaveBtn(sender)
    }
    @IBAction func clickedJustSaveBtn(_ sender: AnyObject) {
        if let _ = movieFileOutput {
            if let _ = assetURL {
                self.revealViewController().rearViewController.performSegue(withIdentifier: "toHomePage", sender: nil)
            } else {
                self.revealViewController().rearViewController.performSegue(withIdentifier: "toHomePage", sender: nil)
            }
        } else {
            UIAlertView(title: "Sharing", message: "Please wait few seconds while we prepare your Explainᴹᴱ", delegate: nil, cancelButtonTitle: "Ok").show()
        }
    }
    
    
    @objc func clickedVeil() {
        videoTitle.becomeFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}

