//
//  RotationHandler.swift
//  Explainme
//
//  Created by Mehdi Kitane on 12/06/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import UIKit

class RotationHandler: NSObject {
    static var previousOrientation : UIInterfaceOrientation = .portrait
    static func interfaceOrientation() -> UIInterfaceOrientation {
        let currentOrientation = UIDevice.current.orientation

        switch currentOrientation {
        case .faceDown:
            return previousOrientation
        case .faceUp:
            return previousOrientation
        case .unknown:
            return previousOrientation
        case .portraitUpsideDown:
            return previousOrientation

        case .portrait:
            previousOrientation = .portrait
            return .portrait
        case .landscapeLeft:
            previousOrientation = .landscapeLeft
            return .landscapeLeft
        case .landscapeRight:
            previousOrientation = .landscapeRight
            return .landscapeRight
        }
    }
    
    static func rotateInterfaceTo(_ views : [UIView], currentOrientation: UIInterfaceOrientation, animate : Bool = true) {
        var rotation : Double = 0;

        switch currentOrientation {
        case .unknown:
            return
        case .portrait:
            previousOrientation = .portrait;
            
            rotation = 0;
            break
        case .portraitUpsideDown:
            //rotation = -M_PI;
            //statusBarOrientation = .PortraitUpsideDown;
            //break
            return
        case .landscapeLeft:
            previousOrientation = .landscapeLeft;
            
            rotation = M_PI_2;
            break;
        case .landscapeRight:
            previousOrientation = .landscapeRight;
            
            rotation = -M_PI_2;
            break
        }

        
        let transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
        if animate {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: UIView.AnimationOptions.beginFromCurrentState, animations: {
                
                for view in views {
                    view.transform = transform
                }
                //UIApplication.shared.setStatusBarOrientation(statusBarOrientation, animated: true)
            }, completion: nil)
        } else {
            for view in views {
                view.transform = transform
            }
            //UIApplication.shared.setStatusBarOrientation(statusBarOrientation, animated: true)
        }
    }
    static func deviceDidRotate(_ views : [UIView], animate : Bool = true) {
        let currentOrientation = UIDevice.current.orientation
        var rotation : Double = 0;
        let statusBarOrientation : UIInterfaceOrientation;
        
        switch currentOrientation {
        case .faceDown:
            return
        case .faceUp:
            return
        case .unknown:
            return
        case .portrait:
            previousOrientation = .portrait;

            rotation = 0;
            statusBarOrientation = .portrait;
            break
        case .portraitUpsideDown:
            //rotation = -M_PI;
            //statusBarOrientation = .PortraitUpsideDown;
            //break
            return
        case .landscapeLeft:
            previousOrientation = .landscapeLeft;

            rotation = M_PI_2;
            statusBarOrientation = .landscapeRight;
            break;
        case .landscapeRight:
            previousOrientation = .landscapeRight;

            rotation = -M_PI_2;
            statusBarOrientation = .landscapeLeft;
            break
        }
        
        let transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
        if animate {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: UIView.AnimationOptions.beginFromCurrentState, animations: {
                
                for view in views {
                    view.transform = transform
                }
                UIApplication.shared.setStatusBarOrientation(statusBarOrientation, animated: true)
            }, completion: nil)
        } else {
            for view in views {
                view.transform = transform
            }
            UIApplication.shared.setStatusBarOrientation(statusBarOrientation, animated: true)
        }
    }
}
