//
//  AudioCaptureExtended.swift
//  Explainme
//
//  Created by Mehdi Kitane on 08/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import AVFoundation

@objc protocol AudioCaptureExtendedDelegate {
    @objc optional func didTalk()
}


class AudioCaptureExtended : AudioCapture {
    var delegate : AudioCaptureExtendedDelegate? = nil
        
    override func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

        //super.captureOutput(captureOutput, didOutputSampleBuffer: sampleBuffer, fromConnection: connection)
        
        let audioChannels = connection.audioChannels
        if audioChannels.count != 0 {
            if let channel = audioChannels[0] as? AVCaptureAudioChannel {
                let ALPHA = 0.04
                let averagePower = pow(10, (ALPHA * Double(channel.averagePowerLevel)))
                //let peakPower = pow(10, (ALPHA * Double(channel.peakHoldLevel)))
                
                if averagePower > 0.10 {
                    delegate?.didTalk?()
                }   
            }
        }

    }

}
