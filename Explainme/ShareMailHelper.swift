//
//  ShareHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 17/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import Foundation
import MessageUI
import UIKit

class ShareMailHelper : NSObject, MFMailComposeViewControllerDelegate {

    
    func saveToEmail(_ subject : String, messageBody: String, urlToSave : URL?, viewController : UIViewController) {
        //if let filePath =  NSBundle.mainBundle().pathForResource("test", ofType: "mov") {
            //let videoURL = NSURL(fileURLWithPath: filePath)
            
        if let videoURL = urlToSave {
            let fileData = try? Data(contentsOf: videoURL)
            if let fileData = fileData {
                let mailComposerVC = MFMailComposeViewController()
                mailComposerVC.mailComposeDelegate = self
                
                // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
                mailComposerVC.setSubject(subject)
                mailComposerVC.setMessageBody(messageBody, isHTML: false)
                mailComposerVC.addAttachmentData(fileData, mimeType: "mp4", fileName: "explainme.mp4")
                
                
                if MFMailComposeViewController.canSendMail() {
                    //We save the completion block so we can use it during our dismiss
                    viewController.present(mailComposerVC, animated: true, completion: nil)
                } else {
                    let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email",
                        message: "Your device could not send e-mail.  Please check e-mail configuration and try again.",
                        delegate: viewController, cancelButtonTitle: "OK")
                    sendMailErrorAlert.show()
                }
            }
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) { () -> Void in
        }
    }
    
    override init() {
        super.init()
    }
}
