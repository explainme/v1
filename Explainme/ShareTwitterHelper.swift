//
//  ShareTwitterHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 16/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import TwitterKit
import MBProgressHUD

class ShareTwitterHelper {
    var hud : MBProgressHUD? = nil

    
    func share(_ text: String, url: URL) {
        let uploadUrl = "https://upload.twitter.com/1.1/media/upload.json"
        let dataVideo : Data = try! Data(contentsOf: url)
        let totalBytes = String(dataVideo.count)
        let videoString : String = dataVideo.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
        
        
        Twitter.sharedInstance().logIn { session, error in
            print("signed in as \(session?.userName)")
            
            
            if let _ = Twitter.sharedInstance().session() {
                print("here \(Twitter.sharedInstance().session())")
                let client : TWTRAPIClient = Twitter.sharedInstance().apiClient
                
                let messageINIT = [
                    "status" : text,
                    "command" : "INIT",
                    "media_type" : "video/mp4",
                    "total_bytes" : totalBytes
                ]
                
                self.startUpload()

                var errorINIT : NSError?
                let preparedRequestINIT = client.urlRequest(withMethod: "POST", url: uploadUrl, parameters: messageINIT, error: &errorINIT)
                client.sendTwitterRequest(preparedRequestINIT, completion: { (urlReponse, responseData, error) -> Void in
                    if( error == nil ) {
                        self.updateUpload(10, 10, 30)
                        print("Succeeeeeded INIT")
                        if let responseData = responseData {
                            do {
                                let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]


                                let mediaID : String = json["media_id_string"] as! String
                                let messageAPPEND = [
                                    "command" : "APPEND",
                                    "media_id" : mediaID,
                                    "segment_index" : "0",
                                    "media" : videoString
                                ]
                                
                                var errorAPPEND : NSError?
                                let preparedRequestAPPEND = client.urlRequest(withMethod: "POST", url: uploadUrl, parameters: messageAPPEND, error: &errorAPPEND)
                                
                                client.sendTwitterRequest(preparedRequestAPPEND, completion: { (urlReponse, responseData, error) -> Void in
                                    self.updateUpload(20, 20, 30)
                                    if( error == nil ) {
                                        print("Succeeeeeded APPEND")
                                        var errorFINALIZE : NSError?
                                        let messageFINALIZE = [
                                            "command" : "FINALIZE",
                                            "media_id" : mediaID
                                        ]
                                        let preparedRequestFINALIZE = client.urlRequest(withMethod: "POST", url: uploadUrl, parameters: messageFINALIZE, error: &errorFINALIZE)
                                        client.sendTwitterRequest(preparedRequestFINALIZE, completion: { (urlReponse, responseData, error) -> Void in
                                            if( error == nil ) {
                                                var errorUpdate : NSError?
                                                // publish video with status
                                                let urlUpdate = "https://api.twitter.com/1.1/statuses/update.json"
                                                let messageUpdate = [
                                                    "status" : text,
                                                    "wrap_links" : "true",
                                                    "media_ids" : mediaID
                                                ]
                                                let preparedRequestUpdate = client.urlRequest(withMethod: "POST", url: urlUpdate, parameters: messageUpdate, error: &errorUpdate)
                                                client.sendTwitterRequest(preparedRequestUpdate, completion: { (urlReponse, responseData, error) -> Void in
                                                    self.updateUpload(30, 30, 30)
                                                    if( error == nil ) {
                                                        do {
                                                            if let responseData = responseData {
                                                                let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)
                                                                print(json)
                                                                self.endUpload(nil)
                                                            }
                                                        } catch {
                                                            
                                                        }
                                                    } else {
                                                        self.endUpload(error as! NSError)
                                                        print("Error command Update: \(error)")
                                                    }
                                                })
                                            } else {
                                                self.endUpload(error as! NSError)
                                                print("Error command FINALIZE: \(error)")
                                            }
                                        })
                                    } else {
                                        self.endUpload(error as! NSError)
                                        print("Error command APPEND: \(error)")
                                    }
                                    
                                })
                            } catch {
                                
                            }
                        }
                        
                    } else {
                        self.endUpload(error as! NSError)
                        print("Error command INIT: \(error)")
                    }
                })
                
            } else {
                self.endUpload(error as! NSError)
                print("error: \(error?.localizedDescription)")
            }
        }
    }

    func startUpload(){
        let window = UIApplication.shared.windows.last
        hud = MBProgressHUD.showAdded(to: window!, animated: true)
        if let hud = hud {
            hud.label.text = "Uploading to Twitter"
            hud.mode = MBProgressHUDMode.determinateHorizontalBar;
        }
    }
    func updateUpload(_ bytesWritten : Int64, _ totalBytesWritten : Int64, _ totalBytesExpectedToWrite: Int64){
        // This closure is NOT called on the main queue for performance
        // reasons. To update your ui, dispatch to the main queue.
        if let hud = self.hud {
            hud.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        }
        /*dispatch_async(dispatch_get_main_queue()) {
        print("Total bytes written on main queue: \(totalBytesWritten)")
        }*/
    }
    func endUpload(_ error: NSError?){
        if let _ = error {
            hud?.mode = MBProgressHUDMode.text
            hud?.label.text = "Oups, An error occured"
            hud?.hide(animated: true, afterDelay: 2)
        } else {
            hud?.hide(animated: true)
        }
        
        //print(response.request)  // original URL request
        //print(response.response) // URL response
        //print(response.data)     // server data
        //print(response.result)   // result of response serialization
        
        /*do {
        let json = try NSJSONSerialization.JSONObjectWithData(response.data!, options: .MutableContainers) as? [String:AnyObject]
        
        //Its done!
        print(json)
        } catch {
        print("Something went wrong")
        }*/
    }

}
