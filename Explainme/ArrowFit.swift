//
//  ArrowFit.swift
//  TestVid
//
//  Created by Mehdi Kitane on 29/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import UIKit


let toleranceArrow : CGFloat = 100

struct ArrowResult {
    var firstPoint: CGPoint
    var lastPoint: CGPoint
    
    init() {
        firstPoint = CGPoint.zero
        lastPoint = CGPoint.zero
    }
}

func fitArrow(_ points: [CGPoint]) -> ArrowResult? {
    if points.count == 0 {
        return nil
    }
    if points.count == 1 {
        return nil
    }
    var result = ArrowResult()
    result.firstPoint = points.first!
    result.lastPoint = points.last!
    
    if points.count == 2 {
        return result
    }
    
    
    for i in 2 ..< points.count {
        let point1 = points[i-2]
        let point2 = points[i-1]

        let currPoint = points[i]
        
        let dxc = currPoint.x - point1.x;
        let dyc = currPoint.y - point1.y;
        
        let dxl = point2.x - point1.x;
        let dyl = point2.y - point1.y;
        
        let cross = dxc * dyl - dyc * dxl;
        if abs(cross) > toleranceArrow {
            return nil
        }
    }
    return result
}
