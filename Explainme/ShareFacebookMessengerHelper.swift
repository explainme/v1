//
//  ShareFacebookMessengerHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 20/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import FBSDKMessengerShareKit

class ShareFacebookMessengerHelper : NSObject {
    
    func saveToMessenger(_ urlToSave : URL?, viewController : UIViewController) {
        //if let filePath =  NSBundle.mainBundle().pathForResource("test", ofType: "mov") {
        //let videoURL = NSURL(fileURLWithPath: filePath)
        
        if let videoURL = urlToSave {
            let fileData = try? Data(contentsOf: videoURL)
            if let fileData = fileData {
                FBSDKMessengerSharer.shareVideo(fileData, with: FBSDKMessengerShareOptions())
                //[FBSDKMessengerSharer shareVideo:videoData withOptions:nil];

            }
        }
    }
}
