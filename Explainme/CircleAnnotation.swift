//
//  CircleAnnotation.swift
//  TestVid
//
//  Created by Mehdi Kitane on 29/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation

class CircleAnnotation : Annotation {
    var center : CGPoint
    var radius : CGFloat
    
    init(center : CGPoint, radius : CGFloat, time : CFTimeInterval) {
        self.center = center
        self.radius = radius
        super.init(time: time)
    }

    
    let firstBorderWidth : CGFloat = 2
    
    override func generateLayer(forSize size : CGSize, forOrientation orientation : VidOrientation, color : UIColor) -> CALayer {
        if referenceSize == CGSize.zero {
            referenceSize = size
        }
        
        var isNotLandscape : CGFloat = 1.0
        if (orientation == .landscapeLeft || orientation == .landscapeRight) {
            isNotLandscape = size.height / size.width
        }

        let factorX : CGFloat = size.width / referenceSize.width
        let factorY : CGFloat = size.height / referenceSize.height

        //position is the position of the center
        var cPos = CGPoint(x: center.x, y: center.y)
        let cRad : CGFloat = factorX * radius * isNotLandscape
        let cBord : CGFloat = factorX * firstBorderWidth * isNotLandscape * sizeFactor
        
        if (orientation == .none) {
            cPos = CGPoint(x: factorX * cPos.x, y: factorY * cPos.y)
        }

        if(orientation == .portrait) {
            cPos = rotatePointToPortrait(cPos, size: size)
        }
        
        if(orientation == .landscapeRight) {
            cPos = rotatePointToRight(cPos, size: size)
        }
        
        if(orientation == .landscapeLeft) {
            cPos = rotatePointToLeft(cPos, size: size)
        }

        let fitRect = CGRect(
            x: cPos.x - cRad,
            y: cPos.y - cRad,
            width: 2 * cRad,
            height: 2 * cRad
        )
        
        let fitRect2 = CGRect(
            x: 2*cBord,
            y: 2*cBord,
            width: 2 * (cRad - 2*cBord),
            height: 2 * (cRad - 2*cBord)
        )

        let fitRect3 = CGRect(
            x: 1*cBord,
            y: 1*cBord,
            width: 2 * (cRad - 1*cBord),
            height: 2 * (cRad - 1*cBord)
        )
        
        
        let overlayLayer = CALayer()
        overlayLayer.frame = fitRect
        overlayLayer.cornerRadius = cRad
        overlayLayer.borderWidth = cBord
        overlayLayer.backgroundColor = UIColor.clear.cgColor
        overlayLayer.masksToBounds = false
        overlayLayer.borderColor = UIColor.white.cgColor
        
        
        let middleOverlayLayer = CALayer()
        middleOverlayLayer.frame = fitRect3
        middleOverlayLayer.cornerRadius = cRad - 1*cBord
        middleOverlayLayer.borderWidth = cBord
        middleOverlayLayer.backgroundColor = UIColor.clear.cgColor
        middleOverlayLayer.masksToBounds = false
        middleOverlayLayer.borderColor = color.cgColor

        
        let innerOverlayLayer = CALayer()
        innerOverlayLayer.frame = fitRect2
        innerOverlayLayer.cornerRadius = cRad - 2*cBord
        innerOverlayLayer.borderWidth = cBord
        innerOverlayLayer.backgroundColor = UIColor.clear.cgColor
        innerOverlayLayer.masksToBounds = false
        innerOverlayLayer.borderColor = UIColor.white.cgColor

        
        overlayLayer.addSublayer(middleOverlayLayer)
        overlayLayer.addSublayer(innerOverlayLayer)
        
        return overlayLayer
    }
}
