//
//  SoundDisplayView.swift
//  Explainme
//
//  Created by Mehdi Kitane on 13/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import UIKit
import AVFoundation

class SoundDisplayView: UIView {

    var totalLength : Double = 0.0 //(In seconds)
    var refreshRate : Int = 35
    var peakPowers : [Double] = []
    var peakTimes : [Double] = []
    
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    func updateView(_ currentTime : Double, audioChannel : AVCaptureAudioChannel) {
        let peak = audioChannel.peakHoldLevel
        
        //Update interface
        //let ALPHA = 0.04
        var peakPower = pow(10, (0.04 * Double(peak)))
        peakPower = -peakPower * 30
        //let peakPower2 = Double(10 * log10(_audioRecorder.averagePowerForChannel(0)));
        
        
        //soundDisplayView.peakPower = peakPoweri
        peakPowers.append(peakPower)
        peakTimes.append(currentTime)
        setNeedsDisplay()
    }
    func updateWithAud(_ audioCapture : AudioCapture, currentTime : Double) {
        let audioOutput = audioCapture.audioOutput
        let connections = audioOutput?.connections
        
        if connections?.count == 0 {
            return
        }
        if connections == nil {
            return
        }
        
        var myAudioChannels : [Any] = []
        for connection in connections! {
            if let connection = connection as? AVCaptureConnection {
                let audioChannels = connection.audioChannels
                if audioChannels.count != 0 {
                    myAudioChannels = audioChannels
                    break;
                }
            }
        }
        
        if myAudioChannels.count == 0 {
            return
        }
        
        if let audioChannel = myAudioChannels[0] as? AVCaptureAudioChannel {
            //let avg = audioChannel.averagePowerLevel
            updateView(currentTime, audioChannel: audioChannel)
        }
    }
    func updateWithVid(_ videoCapture : CaptureObject, currentTime : Double) {
        let videoOutput = videoCapture.outputVideo
        let connections = videoOutput?.connections
        if connections?.count == 0 {
            return
        }
        if connections == nil {
            return
        }

        var myAudioChannels: [Any] = []
        for connection in connections! {
            if let connection = connection as? AVCaptureConnection {
                let audioChannels = connection.audioChannels
                if audioChannels.count != 0 {
                    myAudioChannels = audioChannels
                    break;
                }
            }
        }
        
        if myAudioChannels.count == 0 {
            return
        }
        
        if let audioChannel = myAudioChannels[0] as? AVCaptureAudioChannel {
            //let avg = audioChannel.averagePowerLevel
            updateView(currentTime, audioChannel: audioChannel)
        }

    }
    override func draw(_ rect: CGRect) {
        // Drawing code
        let color:UIColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.90)
        color.set()

        
        for i in 0 ..< peakPowers.count  {
            
            //On echantillonne pour n'en prendre qu'un sur 5
            if i%refreshRate  == 0 {
                let peakPower = peakPowers[i]
                let peakTime = peakTimes[i]
                
                
                let x = (peakTime * Double(rect.width)) / totalLength
                let drect = CGRect(x: x, y: Double(rect.height), width: 1, height: peakPower)

                //let drect = CGRect(x: (w * 0.25),y: (h * 0.25),width: (w * 0.5),height: (h * 0.5))
                let bpath:UIBezierPath = UIBezierPath(rect: drect)
                bpath.fill()
            }

        }


        
       

        
    }
    

}
