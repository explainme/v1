//
//  ShareYoutubeHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 13/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import AeroGearHttp
import AeroGearOAuth2
import Alamofire
import MBProgressHUD

//https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol
//https://developers.google.com/youtube/v3/docs/videos/insert#examples

class ShareYoutubeHelper: NSObject {
    var hud : MBProgressHUD? = nil
    
    func shareToYoutube(_ title: String, description: String, url : URL, privacy: Bool = false) {
        let clientId = "79314346794-5t610c2i36nbtghvtcdv48ahqnvuu716.apps.googleusercontent.com"; //Official
        //let clientId = "79314346794-91dul4oo1gm392k0q9t3oq1oiffvuvqg.apps.googleusercontent.com";
        let scopes : [String] = ["https://www.googleapis.com/auth/youtube.upload",
                                "https://www.googleapis.com/auth/youtube",
                                "https://www.googleapis.com/auth/youtubepartner",
                                "https://www.googleapis.com/auth/youtube.force-ssl"]
        
        
        //Authentication
        let http = Http()
        let googleConfig = GoogleConfig(clientId: clientId, scopes: scopes);
        
        let oauth2Module = AccountManager.addGoogleAccount(config: googleConfig)
        http.authzModule = oauth2Module
        
        oauth2Module.requestAccess { (firstResponse:AnyObject?, error:NSError?) -> Void in
            //print(firstResponse)
            
            self.startUpload()
            if let error = error {
                self.endUpload(error)
                return
            }
            let dic = [
                "snippet" : [
                    "title" : title,
                    "description" : description,
                    "tags" : ["explain.me", "video"]
                ],
                "status" : [
                    "privacyStatus": privacy ? "private" : "public",
                    "embeddable": true,
                    "license": "youtube"
                ]
            ]
            
            let headers = [
                "Authorization": "Bearer " + (firstResponse! as! String),
                "Content-Type": "application/json; charset=UTF-8",
                "X-Upload-Content-Type":"video/*"
            ]
            Alamofire.request("https://www.googleapis.com/upload/youtube/v3/videos?uploadType=resumable&part=snippet,status,contentDetails",
                              method: .post,
                              parameters: dic,
                              encoding: JSONEncoding.default,
                              headers: headers)
                        .response(completionHandler: { (response) in
                            if let error = response.error {
                                self.endUpload(error)
                                return
                            }
                            if let response = response.response {
                                let Resheaders = response.allHeaderFields
                                let location = Resheaders["Location"] //[2] Save the resumable session URI
                                
                                if let location = location as? String {
                                    //[3] Upload the video file
                                    //let fileURL = NSBundle.mainBundle().URLForResource("small-cute-kitten", withExtension: "mp4")
                                    let headersUpload = [
                                        "Authorization": "Bearer " + (firstResponse! as! String),
                                        "Content-Type":"video/*"
                                    ]
                                    
                                    
                                    Alamofire.upload(url, to: location, method: .put, headers: headersUpload)
                                        .uploadProgress(closure: { (progress) in
                                            self.updateUpload(progress.fractionCompleted)
                                        })
                                        .responseJSON { response in
                                            self.endUpload(response.result.error)
                                        }
                                }
                            }
                        })

        }
    }
    
    func startUpload(){
        let window = UIApplication.shared.windows.last
        hud = MBProgressHUD.showAdded(to: window!, animated: true)
        if let hud = hud {
            hud.label.text = "Uploading to Youtube"
            hud.mode = MBProgressHUDMode.determinateHorizontalBar;
        }
    }
    func updateUpload(_ fractionCompleted: Double){
        // This closure is NOT called on the main queue for performance
        // reasons. To update your ui, dispatch to the main queue.
        if let hud = self.hud {
            hud.progress = Float(fractionCompleted)
        }
        /*dispatch_async(dispatch_get_main_queue()) {
            print("Total bytes written on main queue: \(totalBytesWritten)")
        }*/
    }
    
    
    //func endUpload(response: Response<AnyObject, NSError>){
    func endUpload(_ error : Error?){
        if let _ = error {
            hud?.mode = MBProgressHUDMode.text
            hud?.label.text = "Oups, An error occured"
            hud?.hide(animated: true, afterDelay: 2)
        } else {
            hud?.hide(animated: true)
        }

        //print(response.request)  // original URL request
        //print(response.response) // URL response
        //print(response.data)     // server data
        //print(response.result)   // result of response serialization

        /*do {
            let json = try NSJSONSerialization.JSONObjectWithData(response.data!, options: .MutableContainers) as? [String:AnyObject]
            
            //Its done!
            print(json)
        } catch {
            print("Something went wrong")
        }*/
    }
}
