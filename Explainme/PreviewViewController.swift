//
//  PreviewViewController.swift
//  Explainme
//
//  Created by Mehdi Kitane on 13/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//


// TO DO :
// Slider for back and front
// Annotation mal cadrées, à voir
// Voir algorithme Apple pour cadrer image


import UIKit
import AVFoundation
import MBProgressHUD
class PreviewViewController: UIViewController, VideoPlayerDelegate {

    @IBOutlet var globalView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var statusBar: UILabel!
    @IBOutlet weak var middleView: UIView!
    
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var returnButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var videoHolderView: UIView!
    
    
    
    var movieFileOutput: URL!
    var thumbnail : UIImage?

    var videoPlayer : VideoPlayer?
    var annotationPlayer : AnnotationViewPlayer?

    //var soundDisplayView : SoundDisplayView = SoundDisplayView()
    let spinner = UIActivityIndicatorView(style: .white)
    
    //ForExport
    var annotations : [Annotation]?
    var colorForAnnotations : UIColor?
    var savedImage : UIImage?
    var exportFinished = false
    var lastOrientation : UIInterfaceOrientation = UIInterfaceOrientation.unknown
    var photoComingFromLibrary = false

    override func viewDidLoad() {
        super.viewDidLoad()

        RotationHandler.rotateInterfaceTo([checkButton, playButton, returnButton, progressLabel, totalLabel], currentOrientation: lastOrientation, animate: false)
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        videoHolderView.clipsToBounds = true
        videoHolderView.layer.needsDisplayOnBoundsChange = true


        // Do any additional setup after loading the view.
        if (GlobalConstants.mediaChoice == .video) {
            globalView.backgroundColor = GlobalConstants.Colors.pink
            statusBar.backgroundColor = GlobalConstants.Colors.pink
            middleView.backgroundColor = GlobalConstants.Colors.pink
            checkButton.setImage(UIImage(named: "check_btn_red"), for: UIControl.State())
            totalLabel.text = timeStringForTimeInterval(GlobalConstants.RecordingTimes.videoRecordingTime)
        }
        
        progressSlider.minimumTrackTintColor = UIColor.white
        progressSlider.maximumTrackTintColor = GlobalConstants.Colors.clearWhite
        progressSlider.isContinuous = true
        progressSlider.minimumValue = 0
        //Decomment to support sliding 
        //Note AnnotationViewPlayer will not work for now
        //progressSlider.addTarget(self, action: #selector(PreviewViewController.sliderValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        //progressSlider.addTarget(self, action: #selector(PreviewViewController.sliderDidEndDragging(_:)), forControlEvents: UIControlEvents.TouchUpInside)

        
        self.playButton.isHidden = true
        
        //let filePath = NSBundle.mainBundle().pathForResource("small-cute-kitten", ofType: "mp4")
        //movieFileOutput = NSURL(fileURLWithPath: filePath!)
        //Adding the video into the videoHolderView
        
    }
    
    //ForExport
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        func exportVideo(_ mediaChoice : MediaChoice, completion : @escaping () -> Void) {
            let color = colorForAnnotations
            if mediaChoice == .photo {
                let avAsset = AVAsset(url: self.movieFileOutput!)
                let image = self.savedImage!
                
                var orientationToTake = lastOrientation
                if photoComingFromLibrary {
                    //We dont care about the orientation of the device when coming from a photo,
                    //but the photo orientation
                    if image.size.width > image.size.height {
                        orientationToTake = .landscapeLeft
                    } else {
                        orientationToTake = .portrait
                    }
                }
                ExporterSingleton.sharedExporter.exportImage(avAsset,
                                                             image: image,
                                                             annotations: (self.annotations)!,
                                                             annotationColor: color!,
                                                             exportQuality: GlobalConstants.exportQuality,
                                                             lastOrientation: orientationToTake,
                                                             sizeVideo: CGSize(width: 720, height: 1280),
                                                             shouldCropVideoUsingImage: photoComingFromLibrary,
                                                             completion: { (session) -> Void in
                    self.movieFileOutput = session.outputURL
                    completion()
                })
            }
            
            if mediaChoice == .video {
                let avAsset = AVAsset(url: self.movieFileOutput!)
                ExporterSingleton.sharedExporter.exportVideo(avAsset,
                                                             annotations: (self.annotations)!,
                                                             annotationColor: color!,
                                                             exportQuality: GlobalConstants.exportQuality,
                                                             lastOrientation: lastOrientation,
                                                             completion: { (session) -> Void in
                    self.movieFileOutput = session.outputURL
                    completion()
                })
            }
        }
        
        //let window = UIApplication.sharedApplication().windows.last
        
        func exportWaitforExportMethod() {
            DispatchQueue.main.async { () -> Void in
                exportVideo(GlobalConstants.mediaChoice, completion: { () -> Void in
                    self.exportFinished = true
                    self.videoPlayer = VideoPlayer(urlToPlay: self.movieFileOutput)
                    if let videoPlayer = self.videoPlayer {
                        videoPlayer.delegate = self
                        self.progressSlider.maximumValue = videoPlayer.floatDuration!
                        videoPlayer.appendToView(self.videoHolderView)
                    }
                    
                    self.playButton.isHidden = false
                    self.spinner.stopAnimating()
                    self.spinner.removeFromSuperview()
                })
            }
        }
        
        func exportAnnotationPlayerMethod() {
            DispatchQueue.main.async { () -> Void in
                self.exportFinished = true
                exportVideo(GlobalConstants.mediaChoice, completion: { () -> Void in  })
            }
        }

        //exportWaitforExportMethod()
        exportAnnotationPlayerMethod()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        func viewDidAppearWaitforExportMethod() {
            let imageView = UIImageView(image: savedImage)
            imageView.frame = CGRect(x: 0, y: 0, width: videoHolderView.frame.width, height: videoHolderView.frame.height)
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = imageView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            imageView.addSubview(blurEffectView)
            
            spinner.frame = CGRect(x: 0, y: 0, width: videoHolderView.frame.width, height: videoHolderView.frame.height)
            spinner.insertSubview(imageView, at: 0)
            videoHolderView.addSubview(spinner)
            videoHolderView.bringSubviewToFront(spinner)
            spinner.startAnimating()
        }
        
        func viewDidappearAnnotationPlayerMethod() {
            playButton.isHidden = false
            self.videoPlayer = VideoPlayer(urlToPlay: self.movieFileOutput)
            if let videoPlayer = self.videoPlayer {
                videoPlayer.delegate = self
                self.progressSlider.maximumValue = videoPlayer.floatDuration!
                videoPlayer.appendToView(self.videoHolderView)
            }
            
            
            annotationPlayer = AnnotationViewPlayer()
            if let annotationPlayer = annotationPlayer {
                annotationPlayer.photoComingFromLibrary = self.photoComingFromLibrary
                annotationPlayer.annotations = annotations
                annotationPlayer.frame = CGRect(x: 0, y: 0, width: videoHolderView.frame.width, height: videoHolderView.frame.height)

                if GlobalConstants.mediaChoice == .video {
                    annotationPlayer.color = GlobalConstants.Colors.pinkForLayer
                } else {
                    annotationPlayer.image = savedImage
                }
                annotationPlayer.generate()
                videoHolderView.addSubview(annotationPlayer)
            }
            
            func addSpinnerLoading() {
                if GlobalConstants.mediaChoice == .photo {
                    checkButton.setImage(UIImage(named: "clear_btn_blue"), for: UIControl.State())
                } else {
                    checkButton.setImage(UIImage(named: "clear_btn_red"), for: UIControl.State())
                }
                
                spinner.frame = CGRect(x: 0, y: 0, width: checkButton.frame.width, height: checkButton.frame.height)
                checkButton.addSubview(spinner)
                checkButton.bringSubviewToFront(spinner)
                spinner.startAnimating()
            }
            //Just comment this line if you dont want the spinner
            //addSpinnerLoading()
        }
        
        //viewDidAppearWaitforExportMethod()
        viewDidappearAnnotationPlayerMethod()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        playButton.isHidden = false
        if let videoPlayer = videoPlayer {
            videoPlayer.pause(editWasPlaying: true)
            annotationPlayer?.pause()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    @IBAction func clickedCheckButton(_ sender: AnyObject) {
        if exportFinished {
            if self.revealViewController() != nil {
                self.revealViewController().rearViewController.performSegue(withIdentifier: "fromPreviewViewToShareView", sender: self)
            }
        } else {
            UIAlertView(title: "Exporting", message: "Please wait few seconds while we prepare your Explainᴹᴱ", delegate: nil, cancelButtonTitle: "Ok").show()
        }
    }
    
    @IBAction func clickedReturnButton(_ sender: AnyObject) {
        ExporterSingleton.sharedExporter.cancelExport()
        if (GlobalConstants.mediaChoice == .video) {
            self.revealViewController().rearViewController.performSegue(withIdentifier: "toHomePage", sender: nil)
        } else {
            self.revealViewController().rearViewController.performSegue(withIdentifier: "fromCameraViewToExplainView", sender: nil)
        }
    }
    
    
    @IBAction func clickedPlayButtonBG(_ sender: AnyObject) {
        clicked()
    }
    
    @IBAction func clickedPlayButton(_ sender: AnyObject) {
        clicked()
    }
    
    func clicked() {
        if let videoPlayer = videoPlayer {
            if videoPlayer.isPlaying {
                playButton.isHidden = false
                videoPlayer.pause(editWasPlaying: true)
                annotationPlayer?.pause()
            } else {
                playButton.isHidden = true
                videoPlayer.play()
                annotationPlayer?.resume()
            }
        }
    }
    
    
    @IBAction func clickedExplainMeLogo(_ sender: Any) {
        self.revealViewController().rearViewController.performSegue(withIdentifier: "toHomePage", sender: nil)
    }
    // MARK: - UISlider Observers
    func sliderValueChanged(_ slider : UISlider) {
        if let videoPlayer = videoPlayer {
            videoPlayer.pause(editWasPlaying: false)
            videoPlayer.seekToTime(Double(slider.value))
            
            //annotationPlayer?.pause()
            annotationPlayer?.seekToTime(Double(slider.value))
        }
    }
    
    func sliderDidEndDragging(_ slider: UISlider) {
        if let videoPlayer = videoPlayer {
            if videoPlayer.wasPlaying {
                videoPlayer.play()
                
                //annotationPlayer?.resume()
            }
        }
    }
    
    
    // MARK: - VideoPlayerDelegate methods
    func shouldUpdateUI(_ videoPlayer : VideoPlayer) {
        let recorded = CMTimeGetSeconds(videoPlayer.currentTime)
        let max = CMTimeGetSeconds(videoPlayer.duration!)
        progressLabel.text = timeStringForTimeInterval(recorded)
        totalLabel.text = timeStringForTimeInterval(max)
        progressSlider.value = Float(recorded)
    }
    func videoEnded() {
        self.playButton.isHidden = false
        annotationPlayer?.removeAnnotations()
        annotationPlayer?.generate()
    }
}
