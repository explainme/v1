//
//  GlobalConstants.swift
//  Explainme
//
//  Created by Mehdi Kitane on 28/11/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

func degreesToRadians(_ degrees: Double) -> CGFloat {
    return CGFloat(degrees) / 180 * CGFloat(M_PI)
}

func timeStringForTimeInterval(_ secondsTi : Double) -> String {
    let ti = Int(secondsTi)
    let seconds = ti % 60
    let minutes = (ti/60)%60
    let hours = ti/3600
    
    if hours > 0 {
        return String(format: "%02li:%02li:%02li", arguments: [hours, minutes, seconds])
    } else {
        return String(format: "%02li:%02li", arguments: [minutes, seconds])
    }
}

enum MediaChoice : Int {
    case photo = 0, video = 1
}


struct GlobalConstants {
    static var mediaChoice : MediaChoice = .photo
    
    struct Colors {
        static let gray = UIColor(red: 152/255.0, green: 153/255.0, blue: 147/255.0, alpha: 1.0)
        static let pink = UIColor(red: 235/255.0, green: 81/255.0, blue: 109/255.0, alpha: 1.0)
        static let blue = UIColor(red: 24/255.0, green: 142/255.0, blue: 225/255.0, alpha: 1.0)
        static let clearWhite = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.13)
        
        static let blueForLayer = UIColor(red: 24/255.0, green: 142/255.0, blue: 225/255.0, alpha: 0.5)
        static let pinkForLayer = UIColor(red: 235/255.0, green: 81/255.0, blue: 109/255.0, alpha: 0.5)
    }
    
    struct Texts {
        static let picText = "Ready to capture"
        static let vidText = "Ready to record"
        
        static let firstConnexion = "ExplainmeFirstTime"
    }
    
    struct RecordingTimes {
        static let videoRecordingTime : Double = 120 //In seconds
        static let pictureRecordingTime : Double = 60 //InSeconds
    }
    
    struct Durations {
        static let annotationDuration : Double = 1.2
    }
    
    struct Sizes {
        static let videoPreviewSize = CALayerContentsGravity.resizeAspect
        static let imageLayerExportSize = CALayerContentsGravity.resizeAspect
        static let imageSecondViewSize = UIView.ContentMode.scaleAspectFit
        //static let videoPlayerLayerSize = AVLayerVideoGravityResizeAspect
        static let videoPlayerLayerSize = AVLayerVideoGravity.resize
        static let previewLayerCaptureRunningSize = AVLayerVideoGravity.resizeAspectFill
    }
    static let exportQuality = AVAssetExportPreset1920x1080
    //static let exportQuality = AVAssetExportPresetHighestQuality
    //static let exportQuality = AVAssetExportPresetMediumQuality
    //static let exportQuality = AVAssetExportPresetLowQuality
}


func radiansFromUIImageOrientation(_ orientation:UIImage.Orientation) -> Double {
    /*switch (orientation) {
    case UIImageOrientation.Up, UIImageOrientation.UpMirrored:
    return M_PI_2
    case UIImageOrientation.Left, UIImageOrientation.LeftMirrored:
    return 0.0
    case UIImageOrientation.Right, UIImageOrientation.RightMirrored:
    return M_PI
    case UIImageOrientation.Down, UIImageOrientation.DownMirrored:
    return -M_PI_2
    }*/
    switch (orientation) {
    case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
        return 0
    case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
        return -M_PI_2
    case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
        return M_PI_2
    case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
        return M_PI
    }
    
}
func rotateImage(_ originalCGImage : CGImage?, orientation : UIImage.Orientation) -> CGImage? {
    if let originalCGImage = originalCGImage {
        let imageWidth = CGFloat(originalCGImage.width)
        let imageHeight = CGFloat(originalCGImage.height)
        let imageSize = CGSize(width: imageWidth, height: imageHeight);
        
        var rotatedSize : CGSize
        let radians = radiansFromUIImageOrientation(orientation)
        if (radians == M_PI_2 || radians == -M_PI_2) {
            rotatedSize = CGSize(width: imageSize.height, height: imageSize.width)
        } else {
            rotatedSize = imageSize
        }
        
        let rotatedCenterX = rotatedSize.width / 2.0
        let rotatedCenterY = rotatedSize.height / 2.0
        
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, 1.0)
        let rotatedContext = UIGraphicsGetCurrentContext()
        if (radians == 0.0 || radians == M_PI) { // 0 or 180 degrees
            rotatedContext?.translateBy(x: rotatedCenterX, y: rotatedCenterY)
            if (radians == 0.0) {
                rotatedContext?.scaleBy(x: 1.0, y: -1.0)
            } else {
                rotatedContext?.scaleBy(x: -1.0, y: 1.0)
            }
            rotatedContext?.translateBy(x: -rotatedCenterX, y: -rotatedCenterY)
        } else if (radians == M_PI_2 || radians == -M_PI_2) { // +/- 90 degrees
            rotatedContext?.translateBy(x: rotatedCenterX, y: rotatedCenterY)
            rotatedContext?.rotate(by: CGFloat(radians))
            rotatedContext?.scaleBy(x: 1.0, y: -1.0)
            rotatedContext?.translateBy(x: -rotatedCenterY, y: -rotatedCenterX)
        }
        
        let drawingRect = CGRect(x: 0.0, y: 0.0, width: imageSize.width, height: imageSize.height)
        rotatedContext?.draw(originalCGImage, in: drawingRect)
        let rotatedCGImage = rotatedContext?.makeImage()
        
        UIGraphicsEndImageContext()
        return rotatedCGImage
    }
    return nil
}
