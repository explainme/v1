//
//  SideBarTableViewController.swift
//  Explainme
//
//  Created by Mehdi Kitane on 15/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import UIKit

class SideBarTableViewController: UITableViewController {

    let elements = ["Explainᴵᵀ", "How to", "Explainᴹᴱ more"]
    let elementsegues = ["toHomePage", "toTuto", "toHomePage"]
    
    @IBOutlet weak var topSeparator: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //Remove separator in cells at the bottom
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

        topSeparator.frame.size.height = 1 / UIScreen.main.scale
        topSeparator.backgroundColor = self.tableView.separatorColor

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if GlobalConstants.mediaChoice == .video {
            self.view.backgroundColor = GlobalConstants.Colors.pink
        } else {
            self.view.backgroundColor = GlobalConstants.Colors.blue
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var prefersStatusBarHidden : Bool {
        return true;
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return elements.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 73;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSideBar", for: indexPath)

        // Configure the cell...
        if let textLabel = cell.viewWithTag(1) as? UILabel {
            textLabel.text = elements[indexPath.row]
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            showAlert()
            return
        }
        let segueName = elementsegues[indexPath.row]
        self.revealViewController().rearViewController.performSegue(withIdentifier: segueName, sender: nil)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let id = segue.identifier {
            if id == "fromCameraViewToExplainView" {
                if let this = sender as? CameraViewController {
                    if let explainView = segue.destination as? ExplainViewController {
                        if let image = this.savedImage {
                            explainView.image = image
                            explainView.photoComingFromLibrary = this.photoComingFromLibrary
                        }
                    }
                }
            }
            
            if id == "fromCameraViewToRecordView" {
                if let this = sender as? CameraViewController {
                    if let recordView = segue.destination as? RecordViewController {
                        recordView.videoCapture = this.imageCapture
                    }
                }
            }
            
            if id == "fromExplainViewToRecordView" {
                if let this = sender as? ExplainViewController {
                    if let recordView = segue.destination as? RecordViewController {
                        //recordView.mediaChoice = MediaChoice.Photo
                        recordView.savedImage = this.image
                        recordView.photoComingFromLibrary = this.photoComingFromLibrary
                    }
                }
            }
            
            if id == "fromRecordViewToPreviewView" {
                if let this = sender as? RecordViewController {
                    if let previewView = segue.destination as? PreviewViewController {
                        //previewView.mediaChoice = this.mediaChoice
                        previewView.movieFileOutput = this.movieFileOutput
                        
                        //previewView.soundDisplayView.backgroundColor = this.soundDisplayView.backgroundColor
                        //previewView.soundDisplayView.totalLength = this.soundDisplayView.totalLength
                        //previewView.soundDisplayView.peakPowers = this.soundDisplayView.peakPowers
                        //previewView.soundDisplayView.peakTimes = this.soundDisplayView.peakTimes
                        
                        //ForExport
                        previewView.lastOrientation = this.lastOrientation
                        previewView.savedImage = this.savedImage
                        previewView.annotations = this.annotationView.annotations
                        previewView.colorForAnnotations = this.annotationView.colorForAnnotations
                        previewView.photoComingFromLibrary = this.photoComingFromLibrary

                    }
                }
            }

            if id == "fromPreviewViewToShareView" {
                if let this = sender as? PreviewViewController {
                    if let shareView = segue.destination as? ShareViewController {
                        //shareView.mediaChoice = this.mediaChoice
                        //shareView.movieFileOutput = this.movieFileOutput
                        shareView.lastOrientation = this.lastOrientation
                        if let videoPlayer = this.videoPlayer {
                            shareView.thumbnail = videoPlayer.thumbnail
                        }
                        if GlobalConstants.mediaChoice == .photo {
                            shareView.thumbnail = this.savedImage
                        }
                    }
                }
            }

        }
    }
    
    func showAlert() {
        let alertController = UIAlertController(
            title: "", // This gets overridden below.
            message: "",
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(title: "Cancel", style: .cancel) { _ -> Void in }
        alertController.addAction(okAction)
        
        //Custom Title
        let customTitle:NSString = "Explainᴹᴱ more"
        let systemBoldAttributes:[NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont(name: "Lato", size: 17)!
        ]
        let attributedString = NSMutableAttributedString(string: customTitle as String, attributes:systemBoldAttributes)
        alertController.setValue(attributedString, forKey: "attributedTitle")
        
        //CustomMessage
        let customMessage:NSString = "Explainᴹᴱ will soon be released, stay tuned!"
        let systemBoldAttributesMessage:[NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont(name: "Lato", size: 13)!
        ]
        let attributedMessageString = NSMutableAttributedString(string: customMessage as String, attributes:systemBoldAttributesMessage)
        alertController.setValue(attributedMessageString, forKey: "attributedMessage")

        self.present(alertController, animated: true, completion: nil)
    }
}
