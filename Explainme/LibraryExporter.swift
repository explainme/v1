//
//  LibraryExporter.swift
//  Explainme
//
//  Created by Mehdi Kitane on 04/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import Photos

class LibraryExporter {
    static func saveVideoToCameraRoll(_ url : URL, toAlbum name : String, completion : @escaping (_ success: Bool, _ assetURL : URL?) -> Void) {
        
        var videoAssetPlaceholder:PHObjectPlaceholder!
        fetchOrCreateAlbum(name) { (album) -> Void in
            if let album = album {
                PHPhotoLibrary.shared().performChanges({ () -> Void in
                    let assetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
                    videoAssetPlaceholder = assetRequest!.placeholderForCreatedAsset
                    
                    let albumChangeRequest = PHAssetCollectionChangeRequest(for: album)
                    albumChangeRequest!.addAssets([videoAssetPlaceholder!] as NSArray)
                    
                    }, completionHandler: { (success, error) -> Void in
                        if success {
                            
                            let assetURLStr = generateURL(videoAssetPlaceholder)
                            completion(success, URL(string: assetURLStr))
                        }
                })
            } else {
                PHPhotoLibrary.shared().performChanges({ () -> Void in
                    let assetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
                    videoAssetPlaceholder = assetRequest!.placeholderForCreatedAsset

                }, completionHandler: { (success, error) -> Void in
                        print("finishV2")
                        let assetURLStr = generateURL(videoAssetPlaceholder)
                        completion(success, URL(string: assetURLStr))
                })
            }
            
        }
    }
    
    static fileprivate func generateURL(_ videoAssetPlaceholder:PHObjectPlaceholder) -> String {
        let localID = videoAssetPlaceholder.localIdentifier
        let assetID = localID.replacingOccurrences(of: "/.*", with: "",
            options: NSString.CompareOptions.regularExpression, range: nil)
        let ext = "mov"
        let assetURLStr = "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
        return assetURLStr
    }
    
    static fileprivate func fetchOrCreateAlbum(_ name : String, completionHandler : @escaping (_ album : PHAssetCollection?) -> Void){
        let userAlbumsOptions = PHFetchOptions()
        userAlbumsOptions.predicate = NSPredicate(format: "title == %@", name)
        
        let albumResult : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.album, subtype: PHAssetCollectionSubtype.any, options: userAlbumsOptions)
        if albumResult.count == 1 {
            if let album = albumResult.object(at: 0) as? PHAssetCollection {
                completionHandler(album)
            } else {
                completionHandler(nil)
            }
        } else {
            PHPhotoLibrary.shared().performChanges({ () -> Void in
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name)
                }, completionHandler: { (success, error) -> Void in
                    if success {
                        let albumResult2 : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.album, subtype: PHAssetCollectionSubtype.any, options: userAlbumsOptions)
                        if let album2 = albumResult2.object(at: 0) as? PHAssetCollection {
                            completionHandler(album2)
                        } else {
                            completionHandler(nil)
                        }
                    } else {
                        completionHandler(nil)
                    }
            })
        }
        
    }
}
