//
//  ExplainViewController.swift
//  Explainme
//
//  Created by Mehdi Kitane on 30/11/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import UIKit

class ExplainViewController: UIViewController {
    
    @IBOutlet var globalView: UIView!
    @IBOutlet weak var capturedImage:UIImageView!
    var image : UIImage? = nil
    var photoComingFromLibrary = false
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var explainButton: UIButton!
    @IBOutlet weak var returnButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lastOrientation = RotationHandler.interfaceOrientation() //First rotate to take last rotation into account then device didRotate
        RotationHandler.rotateInterfaceTo([explainButton, returnButton], currentOrientation: lastOrientation, animate: false)
        RotationHandler.deviceDidRotate([explainButton, returnButton], animate: false)
        NotificationCenter.default.addObserver(self, selector: #selector(ExplainViewController.deviceDidRotate), name: UIDevice.orientationDidChangeNotification, object: nil)

        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // Do any additional setup after loading the view.
        if var image = image {
            if photoComingFromLibrary {
                if image.size.width > image.size.height {
                    image = UIImage(cgImage: rotateImage(image.cgImage, orientation: UIImage.Orientation.right)!)
                }
            }

            capturedImage.image = image
            writeImage(image, filePath: "explainme_pic.jpeg")
        } else {
            let imageRead = readImage("explainme_pic.jpeg")
            capturedImage.image = imageRead
            image = imageRead
        }
        
        capturedImage.contentMode = GlobalConstants.Sizes.imageSecondViewSize
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    @IBAction func clickedOkButton(_ sender: AnyObject) {
        self.revealViewController().rearViewController.performSegue(withIdentifier: "fromExplainViewToRecordView", sender: self)
    }
    @IBAction func clickedBackButton(_ sender: AnyObject) {
        self.revealViewController().rearViewController.performSegue(withIdentifier: "toHomePage", sender: self)
        //self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func writeImage(_ image: UIImage, filePath: String) {
        let data = image.jpegData(compressionQuality: 1.0)
        let filename = getDocumentsDirectory().appendingPathComponent(filePath)
        if let data = data {
            try? data.write(to: URL(fileURLWithPath: filename), options: [.atomic])
        }
    }
    
    func readImage(_ filePath: String) -> UIImage? {
        let filename = getDocumentsDirectory().appendingPathComponent(filePath)
        return UIImage(contentsOfFile: filename)
    }
    
    @objc func deviceDidRotate() {
        RotationHandler.deviceDidRotate([explainButton, returnButton])
    }
}
