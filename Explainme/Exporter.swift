//
//  Exporter.swift
//  TestVid
//
//  Created by Mehdi Kitane on 27/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

/*let urlpath = NSBundle.mainBundle().pathForResource("lol", ofType: "m4v")
let movieFileOutput = NSURL.fileURLWithPath(urlpath!)
let avAsset = AVAsset(URL: movieFileOutput)*/

import Foundation
import AVFoundation

class Exporter {
    var exporter : AVAssetExportSession?
    
    func exportImage(_ avAsset : AVAsset, image : UIImage, annotations : [Annotation], annotationColor : UIColor, exportQuality : String,
                     lastOrientation: UIInterfaceOrientation, sizeVideo: CGSize, shouldCropVideoUsingImage: Bool, completion: @escaping (_ session: AVAssetExportSession) -> Void ) {
        
            let path = Bundle.main.path(forResource: "blank", ofType: "mp4")
            let trackUrl = URL(fileURLWithPath: path!)
            let asset = AVAsset(url: trackUrl)
        
            
            let mixComposition = createMutableCompositionObjectAndAppendAudioTrack(avAsset)
            let videoTrack = appendVideoTrack(asset, to: mixComposition, duration: avAsset.duration)
            let (mainCompositionInst, videoOrientation, size) = createVideoCompositionForImage(from: videoTrack, avAsset: avAsset, size: sizeVideo, lastOrientation: lastOrientation)
        
            
            applyVideoEffectsToComposition(mainCompositionInst, size: size, annotations: annotations, appendUIImage: image, videoOrientation: videoOrientation, annotationColor: annotationColor)
            let url = Exporter.createOutputUrl("ExplainME.mov")
            export(mixComposition, exportQuality: exportQuality, toURL: url, mainCompositionInst: mainCompositionInst) { (session: AVAssetExportSession) in
                if shouldCropVideoUsingImage {
                    self.cropVideo(url: url, toSize: image.size, exportQuality: exportQuality, completion: { (session2: AVAssetExportSession) in
                        completion(session2)
                    })
                } else {
                    completion(session)
                }
            }
    }
    
    func exportVideo(_ avAsset : AVAsset, annotations : [Annotation], annotationColor : UIColor, exportQuality : String, lastOrientation: UIInterfaceOrientation, completion: @escaping (_ session: AVAssetExportSession) -> Void ) {
        
        let mixComposition = createMutableCompositionObjectAndAppendAudioTrack(avAsset)
        let videoTrack = appendVideoTrack(avAsset, to: mixComposition, duration: avAsset.duration)
        let (mainCompositionInst, videoOrientation, size) = createVideoCompositionForVideo(from: videoTrack, avAsset: avAsset, lastOrientation: lastOrientation)
        
        applyVideoEffectsToComposition(mainCompositionInst, size: size, annotations: annotations, appendUIImage: nil, videoOrientation: videoOrientation, annotationColor: annotationColor)
        let url = Exporter.createOutputUrl("ExplainME.mov")
        export(mixComposition, exportQuality: exportQuality, toURL: url, mainCompositionInst: mainCompositionInst, completion: completion)
    }
    
    fileprivate func applyVideoEffectsToComposition(_ composition : AVMutableVideoComposition, size : CGSize, annotations : [Annotation], appendUIImage image : UIImage?, videoOrientation : VidOrientation, annotationColor: UIColor) {
        
        
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        
        parentLayer.contentsScale = 2.0
        parentLayer.rasterizationScale = 2.0
        parentLayer.shouldRasterize = true

        videoLayer.contentsScale = 2.0
        videoLayer.rasterizationScale = 2.0
        videoLayer.shouldRasterize = true

        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        
        if let image = image {
            var newImageOrientation = image.imageOrientation
            if videoOrientation == .landscapeRight { newImageOrientation = UIImage.Orientation.up }
            if videoOrientation == .landscapeLeft { newImageOrientation = UIImage.Orientation.down }
            
            let newImage = rotateImage(image.cgImage, orientation: newImageOrientation)
            let imageLayer = CALayer()
            imageLayer.contentsScale = 2.0
            imageLayer.rasterizationScale = 2.0
            imageLayer.shouldRasterize = true

            imageLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            imageLayer.contents = newImage
            imageLayer.contentsGravity = GlobalConstants.Sizes.imageLayerExportSize
            parentLayer.addSublayer(imageLayer)
        }
        
        
        for annotation in annotations {
            let overlayLayer = annotation.generateLayer(forSize: size, forOrientation: videoOrientation, color: annotationColor)

            overlayLayer.contentsScale = 2.0
            overlayLayer.rasterizationScale = 2.0
            overlayLayer.shouldRasterize = true

            annotation.appendAnimationToLayer(overlayLayer, atTime: annotation.time)
            parentLayer.addSublayer(overlayLayer)
        }
        composition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
    }
    
    
    
    
    
    fileprivate func createMutableCompositionObjectAndAppendAudioTrack(_ avAsset: AVAsset) -> AVMutableComposition {
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        
        // 2 - add Audio Track
        let audioTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: 0)
        do {
            try audioTrack!.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: avAsset.duration), of: avAsset.tracks(withMediaType: AVMediaType.audio)[0], at: CMTime.zero)
        } catch {
            
        }
        
        return mixComposition
    }
    
    fileprivate func appendVideoTrack(_ avAsset : AVAsset, to mixComposition : AVMutableComposition, duration : CMTime) -> AVMutableCompositionTrack {
        let videoTrack : AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        do {
            try videoTrack.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: duration), of: avAsset.tracks(withMediaType: AVMediaType.video)[0], at: CMTime.zero)
        } catch {
            
        }
        
        return videoTrack
    }
    
    static func createOutputUrl(_ name : String) -> URL{
        // 4 - Get path
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let documentDirectory = paths[0]
        let savePath = (documentDirectory as NSString).appendingPathComponent(name)
        let exportURL =  URL(fileURLWithPath: savePath)
        
        do {
            if FileManager.default.fileExists(atPath: exportURL.path) {
                try FileManager.default.removeItem(atPath: exportURL.path)
            }
        } catch {
            
        }
        return exportURL
        //return NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent(name)
    }
    
    fileprivate func export(_ mixComposition : AVMutableComposition, exportQuality : String, toURL url : URL, mainCompositionInst : AVMutableVideoComposition, completion : @escaping (_ session: AVAssetExportSession) -> Void) {
        
        exporter = AVAssetExportSession(asset: mixComposition, presetName: exportQuality)
        if let exporter = exporter {
            exporter.outputURL = url
            exporter.outputFileType = AVFileType.mov
            exporter.shouldOptimizeForNetworkUse = false
            exporter.videoComposition = mainCompositionInst
            
            // 6 - Perform the Export
            exporter.exportAsynchronously() {
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(exporter)
                })
            }
        }
        
    }
    fileprivate func export(_ mixComposition : AVAsset, exportQuality : String, toURL url : URL, mainCompositionInst : AVMutableVideoComposition, completion : @escaping (_ session: AVAssetExportSession) -> Void) {
        
        exporter = AVAssetExportSession(asset: mixComposition, presetName: exportQuality)
        if let exporter = exporter {
            exporter.outputURL = url
            exporter.outputFileType = AVFileType.mov
            exporter.shouldOptimizeForNetworkUse = false
            exporter.videoComposition = mainCompositionInst
            
            // 6 - Perform the Export
            exporter.exportAsynchronously() {
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(exporter)
                })
            }
        }
        
    }

    
    //Different for Image and Video
    fileprivate func createVideoCompositionForVideo(from videoTrack : AVAssetTrack, avAsset : AVAsset, lastOrientation: UIInterfaceOrientation) -> (AVMutableVideoComposition, VidOrientation, CGSize)  {
        // 3.1 - Create AVMutableVideoCompositionInstruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: avAsset.duration)
        
        // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
        let videoLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
        let videoAssetTrack = avAsset.tracks(withMediaType: AVMediaType.video)[0]
        videoLayerInstruction.setOpacity(0.0, at: avAsset.duration)

        
        var videoOrientation : VidOrientation = VidOrientation.none
        var isVideoAssetPortrait = false
        if (lastOrientation == .portrait) {
            videoLayerInstruction.setTransform(videoAssetTrack.preferredTransform, at: CMTime.zero)
            videoOrientation = VidOrientation.portrait
            isVideoAssetPortrait = true
        }
        if (lastOrientation == .landscapeRight) {
            let t1 = CGAffineTransform(rotationAngle: degreesToRadians(180.0))
            let finalTransform = t1.translatedBy(x: -videoAssetTrack.naturalSize.width, y: -videoAssetTrack.naturalSize.height)
            videoLayerInstruction.setTransform(finalTransform, at: CMTime.zero)

            videoOrientation = VidOrientation.landscapeLeft
            isVideoAssetPortrait = false
        }
        if (lastOrientation == .landscapeLeft) {
            videoOrientation = VidOrientation.landscapeRight
            isVideoAssetPortrait = false
        }
        
        var naturalSize : CGSize
        if isVideoAssetPortrait {
            naturalSize = CGSize(width: videoAssetTrack.naturalSize.height, height: videoAssetTrack.naturalSize.width)
        } else {
            naturalSize = videoAssetTrack.naturalSize
        }
        
        //Bug fix
        var renderWidth = naturalSize.width
        var renderHeight = naturalSize.height
        while( (renderWidth.truncatingRemainder(dividingBy: 16)) > 0) {
            renderWidth = renderWidth + 1
        }
        while( (renderHeight.truncatingRemainder(dividingBy: 16)) > 0) {
            renderHeight = renderHeight + 1
        }

        
        // 3.3 - Add instructions
        mainInstruction.layerInstructions = [videoLayerInstruction]
        let mainCompositionInst = AVMutableVideoComposition()
        mainCompositionInst.renderSize = CGSize(width: renderWidth, height: renderHeight)
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.frameDuration = CMTimeMake(value: 1, timescale: 30) // FPS
        
        return (mainCompositionInst, videoOrientation, CGSize(width: renderWidth, height: renderHeight))
    }
    
    fileprivate func createVideoCompositionForImage(from videoTrack : AVAssetTrack, avAsset : AVAsset, size : CGSize, lastOrientation: UIInterfaceOrientation) -> (AVMutableVideoComposition, VidOrientation, CGSize) {
        // 3.1 - Create AVMutableVideoCompositionInstruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: avAsset.duration)
        
        // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
        let videoLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
        videoLayerInstruction.setOpacity(0.0, at: avAsset.duration)
        
        var videoOrientation : VidOrientation = VidOrientation.none
        var isVideoAssetPortrait = false
        if (lastOrientation == .portrait) {
            videoOrientation = VidOrientation.portrait
            isVideoAssetPortrait = true
        }
        if (lastOrientation == .landscapeRight) {
            videoOrientation = VidOrientation.landscapeLeft
            isVideoAssetPortrait = false
        }
        if (lastOrientation == .landscapeLeft) {
            videoOrientation = VidOrientation.landscapeRight
            isVideoAssetPortrait = false
        }
        
        var naturalSize : CGSize
        if !isVideoAssetPortrait {
            naturalSize = CGSize(width: size.height, height: size.width)
        } else {
            naturalSize = size
        }

        //Bug fix
        var renderWidth = naturalSize.width
        var renderHeight = naturalSize.height
        while( (renderWidth.truncatingRemainder(dividingBy: 16)) > 0) {
            renderWidth = renderWidth + 1
        }
        while( (renderHeight.truncatingRemainder(dividingBy: 16)) > 0) {
            renderHeight = renderHeight + 1
        }

        // 3.3 - Add instructions
        mainInstruction.layerInstructions = [videoLayerInstruction]
        
        let mainCompositionInst = AVMutableVideoComposition()
        mainCompositionInst.renderSize = naturalSize
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        return (mainCompositionInst, videoOrientation, CGSize(width: renderWidth, height: renderHeight))
    }
    
    //https://gist.github.com/zrxq/9817265
    fileprivate func cropVideo(url: URL, toSize: CGSize, exportQuality: String, completion : @escaping (_ session: AVAssetExportSession) -> Void) {
        let asset = AVAsset(url: url)
        guard let assetVideoTrack = asset.tracks(withMediaType: AVMediaType.video).last else { return }

        let originalSize = assetVideoTrack.naturalSize
        

        var finalSizeWidth: CGFloat = CGFloat(0)
        var finalSizeHeight: CGFloat = CGFloat(0)
        var finalMoveWidth: CGFloat = CGFloat(0)
        var finalMoveHeight: CGFloat = CGFloat(0)
        if originalSize.width > originalSize.height {
            //For landscape pictures
            finalSizeWidth = toSize.width * (originalSize.height/toSize.height)
            finalSizeHeight = originalSize.height
            
            finalMoveWidth = -1 * ((originalSize.width - finalSizeWidth)/2)
            finalMoveHeight = CGFloat(0)
        } else {
            //For portrait pictures
            finalSizeHeight = toSize.height * (originalSize.width/toSize.width)
            finalSizeWidth = originalSize.width
            
            finalMoveHeight = -1 * ((originalSize.height - finalSizeHeight)/2)
            finalMoveWidth = CGFloat(0)
        }


        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: assetVideoTrack)
        let orientationTransform = assetVideoTrack.preferredTransform
        

        /*Fix orientation error*/
        let transform = orientationTransform.translatedBy(x: finalMoveWidth, y: finalMoveHeight)
        layerInstruction.setTransform(transform, at: CMTime.zero)
        
        /*Instructions*/
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.layerInstructions = [ layerInstruction ]
        instruction.timeRange = assetVideoTrack.timeRange
        
        /*VideoComposition*/
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = CGSize(width: finalSizeWidth, height: finalSizeHeight)
        videoComposition.renderScale = 1.0
        videoComposition.frameDuration = CMTimeMake(value: 1,timescale: 30)
        videoComposition.instructions = [ instruction ]
        
        /*Export*/
        let url = Exporter.createOutputUrl("ExplainME2.mov")
        export(asset, exportQuality: exportQuality, toURL: url, mainCompositionInst: videoComposition) { (exportSession) in
            completion(exportSession)
        }
    }
}
