//
//  TutorialViewController.swift
//  Explainme
//
//  Created by Mehdi Kitane on 14/11/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var goButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        goButton.layer.cornerRadius = 24.5
        goButton.clipsToBounds = true
        goButton.backgroundColor = UIColor.white
    }


    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func clickedLetsGo(_ sender: AnyObject) {
        self.revealViewController().rearViewController.performSegue(withIdentifier: "fromTutoToTutoCamera", sender: nil)
        //self.performSegueWithIdentifier("showTutorialCamera", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
