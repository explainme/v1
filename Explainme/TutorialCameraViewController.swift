//
//  TutorialCameraViewController.swift
//  Explainme
//
//  Created by Mehdi Kitane on 07/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import UIKit

enum TutorialStates {
    case none, voice, point, highlight, indicate, done
}

extension UIView {
    func animateZoomInZoomOut(_ lengthAnimation : Double, completion: (() -> Void)?) {
        
        self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0);
        UIView.animate(withDuration: lengthAnimation, animations: { () -> Void in
                self.transform = CGAffineTransform.identity.scaledBy(x: 1.2, y: 1.2)
            }, completion: { (completed) -> Void in
                UIView.animate(withDuration: lengthAnimation, animations: { () -> Void in
                    self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0);
                    }, completion: { (completed) -> Void in
                        
                })
        }) 

    }
}

class TutorialCameraViewController: UIViewController, AnnotationViewDelegate, AudioCaptureExtendedDelegate {

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var mainStatusBar: UILabel!
    @IBOutlet weak var bottomStatusBar: UILabel!
    var annotationView : AnnotationView!

    var audioCapture : AudioCaptureExtended?
    var imageCapture : CaptureObject?
    fileprivate var currentState = TutorialStates.voice
    
    @IBOutlet weak var voiceImage: UIImageView!
    @IBOutlet weak var pointImage: UIImageView!
    @IBOutlet weak var highlightImage: UIImageView!
    @IBOutlet weak var indicateImage: UIImageView!
    @IBOutlet weak var menuButton: UIButton!
    
    fileprivate var alreadyExitedTutorial = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        annotationView = AnnotationView()
        annotationView.delegate = self
        annotationView.colorForAnnotations = GlobalConstants.Colors.blueForLayer

        CaptureObject.checkDeviceAuthorizationStatus { (grantedVideo, firstTimeVideo) in
            AudioCapture.checkDeviceAuthorizationStatus({ (grantedAudio, firstTimeAudio) in
                if !grantedVideo {
                    let message = "Express yourself using Explain.me. Allow access to your camera and microphone to start using the Explain.me app."
                    let alert = UIAlertController(title:"No Camera", message:message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:"Change settings", style:UIAlertAction.Style.default, handler:{ action in
                        UIApplication.shared.openURL(URL(string:"prefs:root=Privacy&path=CAMERA")!)

                    }))
                    DispatchQueue.main.async(execute: { 
                        self.present(alert, animated:true, completion:nil)
                    })
                    return
                } else if !grantedAudio {
                    let message = "Express yourself using Explain.me. Allow access to your camera and microphone to start using the Explain.me app."
                    let alert = UIAlertController(title:"No Microphone", message:message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:"Change settings", style:UIAlertAction.Style.default, handler:{ action in
                        UIApplication.shared.openURL(URL(string:"prefs:root=Privacy&path=MICROPHONE")!)
                    }))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated:true, completion:nil)
                    })
                    return
                }
                
                if grantedVideo {
                    print("grantedvideo")
                    self.imageCapture = CaptureObject()
                }
                if grantedAudio && grantedVideo {
                    print("grantedaudio")
                    self.audioCapture = AudioCaptureExtended()
                }
                if firstTimeVideo {
                    self.doWhenViewWillAppearVideo()
                }
                if firstTimeAudio {
                    self.doWhenViewWillAppearAudio()
                }
            })
        }
        
        
        mainStatusBar.animateZoomInZoomOut(0.5, completion: nil)
        
        currentState = TutorialStates.none
        mainStatusBar.text = "You can express yourself \n in 4 simple ways"
        bottomStatusBar.text = "Explain yourself. Talk and Point."
        
        
        let delay = 4.0
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            self.currentState = TutorialStates.voice
            self.mainStatusBar.text = "First let's hear your voice. \n Say something!"
            self.bottomStatusBar.text = "When starting, we record your voice"
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        doWhenViewWillAppearVideo()
        doWhenViewWillAppearAudio()
        annotationView.frame = cameraView.bounds
        cameraView.addSubview(annotationView)
    }
    
    func doWhenViewWillAppearVideo() {
        CaptureObject.cameraQueue.async { () -> Void in
            print("will init audio && vid 1")
            self.imageCapture?.willRotateToInterfaceOrientation(UIApplication.shared.statusBarOrientation, duration: 0)
            self.imageCapture?.startRunning()
            DispatchQueue.main.async(execute: {
                self.imageCapture?.append(inView: self.cameraView, atlayerIndex: 0)
                print("will end init audio && vid 1")
            })
        }
    }
    func doWhenViewWillAppearAudio() {
        audioCapture?.delegate = self
        audioCapture?.shouldWriteToFile = false
        audioCapture?.startAudio()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        imageCapture?.stopRunning()
        audioCapture?.stopAudio()
    }
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    

    @objc func exitTutorial() {
        UserDefaults.standard.set(true, forKey: GlobalConstants.Texts.firstConnexion)
        //self.performSegueWithIdentifier("exitTutorial", sender: nil)
        if alreadyExitedTutorial == false {
            alreadyExitedTutorial = true
            self.revealViewController().rearViewController.performSegue(withIdentifier: "toHomePage", sender: nil)
        }
    }
    
    @IBAction func clickedSkip(_ sender: AnyObject) {
        exitTutorial()
    }
    
    // MARK: - Annotation delegates
    func getTimeForAnnotation() -> CFTimeInterval {
        return 0
    }
    func didDrawAnArrow() {
        if currentState == .indicate {
            currentState = .done
            DispatchQueue.main.async(execute: { () -> Void in
                self.indicateImage.image = UIImage(named: "IndicateDone")
                self.indicateImage.animateZoomInZoomOut(0.3, completion: nil)
                self.mainStatusBar.text = "Nice! You're all set. \n We'll take you into the app."
                self.bottomStatusBar.text = "Just draw a line and we'll show it as an arrow"
                
                
                self.perform(#selector(TutorialCameraViewController.exitTutorial), with: nil, afterDelay: 2)
            })
        }
    }
    func didDrawACircle() {
        if currentState == .highlight {
            currentState = .indicate
            DispatchQueue.main.async(execute: { () -> Void in
                self.highlightImage.image = UIImage(named: "HighlightDone")
                self.highlightImage.animateZoomInZoomOut(0.3, completion: nil)
                self.mainStatusBar.text = "Highlighted. \n Last indicate by drawing a line."
                self.bottomStatusBar.text = "Just draw a line and we'll show it as an arrow"
            })
        }
    }
    func didDrawAPoint() {
        if currentState == .point {
            currentState = .highlight
            DispatchQueue.main.async(execute: { () -> Void in
                self.pointImage.image = UIImage(named: "PointDone")
                self.pointImage.animateZoomInZoomOut(0.3, completion: nil)
                self.mainStatusBar.text = "Good job, your point is clear. \n Next, highlight by drawing a circle."
                self.bottomStatusBar.text = "You can highlight as big as you want"
            })
        }
    }
    
    func didTalk() {
        if currentState == .voice {
            self.currentState = .point
            DispatchQueue.main.async(execute: { () -> Void in
                self.voiceImage.image = UIImage(named: "VoiceDone")
                self.voiceImage.animateZoomInZoomOut(0.3, completion: nil)
                self.mainStatusBar.text = "We hear you loud and clear! \n Next, point on something."
                self.bottomStatusBar.text = "The fastest way to point is by placing a dot"
            })
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
