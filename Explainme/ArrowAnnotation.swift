//
//  ArrowAnnotation.swift
//  TestVid
//
//  Created by Mehdi Kitane on 29/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation

class ArrowAnnotation : Annotation {
    var firstPoint : CGPoint
    var lastPoint : CGPoint
    
    init(firstPoint : CGPoint, lastPoint : CGPoint, time : CFTimeInterval) {
        self.firstPoint = firstPoint
        self.lastPoint = lastPoint
        super.init(time: time)
    }
    
    let borderWidth : CGFloat = 2
    let arrowWidth : CGFloat = 5
    let headLength : CGFloat = 20
    let headWidth : CGFloat = 5*3 //arrowWidth*3
    
    
    override func generateLayer(forSize size : CGSize, forOrientation orientation : VidOrientation, color : UIColor) -> CALayer {
        if referenceSize == CGSize.zero {
            referenceSize = size
        }
        
        var isNotLandscape : CGFloat = 1.0
        if (orientation == .landscapeLeft || orientation == .landscapeRight) {
            isNotLandscape = size.height / size.width
        }

        let factorX : CGFloat = size.width / referenceSize.width
        let factorY : CGFloat = size.height / referenceSize.height

        var cPoint1 : CGPoint = CGPoint(x: firstPoint.x, y: firstPoint.y)
        var cPoint2 : CGPoint = CGPoint(x: lastPoint.x, y: lastPoint.y)
        let cBord : CGFloat = factorX * borderWidth * isNotLandscape
        let cArrowWidth : CGFloat = factorX * arrowWidth * isNotLandscape * sizeFactor
        let cHeadWidth = factorX * headWidth * isNotLandscape * sizeFactor
        let cHeadLength = factorX * headLength * isNotLandscape * sizeFactor
    
        if (orientation == .none) {
            cPoint1 = CGPoint(x: factorX * cPoint1.x, y: factorY * cPoint1.y)
            cPoint2 = CGPoint(x: factorX * cPoint2.x, y: factorY * cPoint2.y)
        }

        if(orientation == .portrait) {
            cPoint1 = rotatePointToPortrait(cPoint1, size: size)
            cPoint2 = rotatePointToPortrait(cPoint2, size: size)
        }
        
        if(orientation == .landscapeRight) {
            cPoint1 = rotatePointToRight(cPoint1, size: size)
            cPoint2 = rotatePointToRight(cPoint2, size: size)
        }

        if(orientation == .landscapeLeft) {
            cPoint1 = rotatePointToLeft(cPoint1, size: size)
            cPoint2 = rotatePointToLeft(cPoint2, size: size)
        }
        
        var finalPoint : CGPoint = CGPoint.zero
        finalPoint.x = cPoint1.x < cPoint2.x ? cPoint1.x : cPoint2.x
        finalPoint.y = cPoint1.y < cPoint2.y ? cPoint1.y : cPoint2.y
        var arrowSize : CGSize = CGSize.zero
        arrowSize.width = abs(cPoint1.x - cPoint2.x)
        arrowSize.height = abs(cPoint1.y - cPoint2.y)
        
        let overlayLayer = CAShapeLayer()
        overlayLayer.frame = CGRect(x: finalPoint.x, y: finalPoint.y, width: arrowSize.width, height: arrowSize.height)
        overlayLayer.backgroundColor = UIColor.clear.cgColor
        overlayLayer.masksToBounds = false
        
        
        //ArrowShape : 
        //let mutPath : CGMutablePathRef = CGPathCreateMutable()
        var startPoint = CGPoint.zero
        var endPoint = CGPoint.zero
        startPoint.x = cPoint1.x < cPoint2.x ? 0 : overlayLayer.frame.width
        endPoint.x = cPoint1.x < cPoint2.x ? overlayLayer.frame.width : 0

        startPoint.y = cPoint1.y < cPoint2.y ? 0 : overlayLayer.frame.height
        endPoint.y = cPoint1.y < cPoint2.y ? overlayLayer.frame.height : 0
        
        
        let path = UIBezierPath.bezierPathWithArrowFromPoint(startPoint, endPoint: endPoint, tailWidth: cArrowWidth, headWidth: cHeadWidth, headLength: cHeadLength)
        
        /*let path = UIBezierPath()
        path.moveToPoint(startPoint)
        path.addLineToPoint(endPoint)*/
        

        overlayLayer.fillColor = color.cgColor
        overlayLayer.strokeColor = UIColor.white.cgColor
        overlayLayer.lineWidth = cBord
        overlayLayer.path = path.cgPath
        
        return overlayLayer
    }
    
}


extension UIBezierPath {
    
    class func getAxisAlignedArrowPoints(_ points: inout Array<CGPoint>, forLength: CGFloat, tailWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat ) {
        
        let tailLength = forLength - headLength
        points.append(CGPoint(x: 0, y: tailWidth/2))
        points.append(CGPoint(x: tailLength, y: tailWidth/2))
        points.append(CGPoint(x: tailLength, y: headWidth/2))
        points.append(CGPoint(x: forLength, y: 0))
        points.append(CGPoint(x: tailLength, y: -headWidth/2))
        points.append(CGPoint(x: tailLength, y: -tailWidth/2))
        points.append(CGPoint(x: 0, y: -tailWidth/2))
        
    }
    
    
    class func transformForStartPoint(_ startPoint: CGPoint, endPoint: CGPoint, length: CGFloat) -> CGAffineTransform{
        let cosine: CGFloat = (endPoint.x - startPoint.x)/length
        let sine: CGFloat = (endPoint.y - startPoint.y)/length
        
        return CGAffineTransform(a: cosine, b: sine, c: -sine, d: cosine, tx: startPoint.x, ty: startPoint.y)
    }
    
    
    class func bezierPathWithArrowFromPoint(_ startPoint:CGPoint, endPoint: CGPoint, tailWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath {
        
        let xdiff: Float = Float(endPoint.x) - Float(startPoint.x)
        let ydiff: Float = Float(endPoint.y) - Float(startPoint.y)
        let length = hypotf(xdiff, ydiff)
        
        var points = [CGPoint]()
        self.getAxisAlignedArrowPoints(&points, forLength: CGFloat(length), tailWidth: tailWidth, headWidth: headWidth, headLength: headLength)
        
        var transform: CGAffineTransform = self.transformForStartPoint(startPoint, endPoint: endPoint, length:  CGFloat(length))
        
        let cgPath: CGMutablePath = CGMutablePath()
        cgPath.addLines(between: points, transform: transform)
        cgPath.closeSubpath()
        
        let uiPath: UIBezierPath = UIBezierPath(cgPath: cgPath)
        return uiPath
    }
}
