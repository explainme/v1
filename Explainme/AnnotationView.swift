//
//  AnnotationView.swift
//  
//
//  Created by Mehdi Kitane on 29/02/16.
//
//

import UIKit

class AnnotationView: UIView {
    var annotations : [Annotation] = []
    var delegate : AnnotationViewDelegate? = nil
    var colorForAnnotations : UIColor = UIColor.clear
    // Special CircleDrawView
    var path: CGPath?
    var fitResult: CircleResult?
    // End Special Draw View
    

    init() {
        super.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.clear
        
        
        //Add our gestures
        let circleRecognizer = MKGestureRecognizer(target: self, action: #selector(AnnotationView.circled(_:)))
        self.addGestureRecognizer(circleRecognizer)
    }
    
    @objc func circled(_ c: MKGestureRecognizer) {
        if c.state == .began {
            self.clear()
        }
        if c.state == .changed {
            self.updatePath(c.path)
        }

        if c.state == .failed {
            print("Failed")
            print(c.path)
        }
        
        if c.state == .cancelled {
            print("Cancelled")
            print(c.path)
        }

        if c.state == .ended {
            //retrieveTimeForAnnotation
            var time : CFTimeInterval = -1
            if let delegate = delegate {
                time = delegate.getTimeForAnnotation()
            }
            
            self.clear()
            // now that the user has stopped touching, figure out if the path was a circle
            let fitResult = fitCircle(c.touchedPoints)

            // make sure there are no points in the middle of the circle
            let hasInside = anyPointsInTheMiddle(fitResult, touchedPoints: c.touchedPoints)
            let percentOverlap = calculateBoundingOverlap(fitResult, path: c.path)
            let isCircle = fitResult.error <= toleranceCircle && !hasInside && percentOverlap > (1-toleranceCircle)


            var annotation : Annotation? = nil
            if isCircle {
                
                annotation = CircleAnnotation(center: fitResult.center, radius: fitResult.radius, time: time)
                delegate?.didDrawACircle?()
            } else if c.touchedPoints.count <= 7 {
                
                let touchPoint = c.location(in: self)
                annotation = PointAnnotation(x: touchPoint.x, y: touchPoint.y, time: time)
                delegate?.didDrawAPoint?()
            } else if let fitArrowResult = fitArrow(c.touchedPoints) {
                
                annotation = ArrowAnnotation(firstPoint: fitArrowResult.firstPoint, lastPoint: fitArrowResult.lastPoint, time: time)
                delegate?.didDrawAnArrow?()
            }
            
            if let annotation = annotation {
                annotations.append(annotation)
                let overlay = annotation.generateLayer(forSize: self.frame.size, forOrientation: .none, color: colorForAnnotations)
                annotation.appendAnimationToLayer(overlay, atTime: 0.0)
                self.layer.addSublayer(overlay)
                perform(#selector(AnnotationView.removeSublayer(_:)), with: overlay, afterDelay : 1)
            }            
        }
    }
    

    @objc func removeSublayer(_ layer : CALayer) {
        layer.removeFromSuperlayer()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //To Do : Remove
    /*func simulateAnnotation(point: AnyObject?) {
        if let point = point as? Annotation {
            annotations.append(point)
            
            let overlayLayer = point.generateLayer(forSize: UIScreen.mainScreen().bounds.size, forOrientation: .None)
            point.appendAnimationToLayer(overlayLayer, atTime: 0.0)
            
            self.layer.addSublayer(overlayLayer)
            performSelector(Selector("removeSublayer:"), withObject: overlayLayer, afterDelay : 1)
            
            print(self.layer.sublayers)
        }
    }*/
    
    
    
    //Draw Rect methods 
    func updateFit(_ fit: CircleResult?) {
        fitResult = fit
        setNeedsDisplay()
    }
    
    func updatePath(_ p: CGPath?) {
        path = p
        setNeedsDisplay()
    }
    
    func clear() {
        updateFit(nil)
        updatePath(nil)
    }
    override func draw(_ rect: CGRect) {
        /*if let path = path { // draw a thick yellow line for the user touch path
            let context = UIGraphicsGetCurrentContext()
            CGContextAddPath(context, path)
            CGContextSetStrokeColorWithColor(context, colorForAnnotations.CGColor)
            CGContextSetLineWidth(context, 3)
            CGContextSetLineCap(context, CGLineCap.Round)
            CGContextSetLineJoin(context, CGLineJoin.Round)
            CGContextStrokePath(context)
        }*/
    }
}
