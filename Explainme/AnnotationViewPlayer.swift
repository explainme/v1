//
//  AnnotationViewPlayer.swift
//  Explainme
//
//  Created by Mehdi Kitane on 29/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import UIKit

class AnnotationViewPlayer: UIView {

    var annotations : [Annotation]?
    var color : UIColor = GlobalConstants.Colors.blueForLayer
    var image : UIImage?
    var photoComingFromLibrary = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func generate() {
        if var image = image {
            let newImageOrientation = image.imageOrientation
            //if videoOrientation == .LandscapeRight { newImageOrientation = UIImageOrientation.Up }
            //if videoOrientation == .LandscapeLeft { newImageOrientation = UIImageOrientation.Down }

            var newImage = rotateImage(image.cgImage, orientation: image.imageOrientation)
            if photoComingFromLibrary {
                if image.size.width > image.size.height {
                    image = UIImage(cgImage: rotateImage(image.cgImage, orientation: UIImage.Orientation.right)!)
                    newImage = image.cgImage
                }
            }
            
            let imageLayer = CALayer()
            imageLayer.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
            imageLayer.contents = newImage
            imageLayer.contentsGravity = GlobalConstants.Sizes.imageLayerExportSize
            self.layer.addSublayer(imageLayer)
        }
        
        if let annotations = annotations {
            for annotation in annotations {
                let overlayLayer = annotation.generateLayer(forSize: self.frame.size, forOrientation: .none, color: color)
                annotation.appendAnimationToLayer(overlayLayer, atTime: annotation.time, isExport: false)
                self.layer.addSublayer(overlayLayer)
            }
            pause()
        }
    }
    
    func pause() {
        if let sublayers = self.layer.sublayers {
            for overlayLayer in sublayers {
                let pausedTime = overlayLayer.convertTime(CACurrentMediaTime(), from: nil)
                overlayLayer.speed = 0.0;
                overlayLayer.timeOffset = pausedTime;
            }
        }
    }
    
    func resume() {
        if let sublayers = self.layer.sublayers {
            for overlayLayer in sublayers {
                let pausedTime = overlayLayer.timeOffset
                overlayLayer.speed = 1.0;
                overlayLayer.timeOffset = 0.0;
                overlayLayer.beginTime = 0.0;
                let timeSincePause = overlayLayer.convertTime(CACurrentMediaTime(), from:nil) - pausedTime
                overlayLayer.beginTime = timeSincePause;
            }
        }
    }
    
    func removeAnnotations() {
        if let sublayers = self.layer.sublayers {
            for overlayLayer in sublayers {
                overlayLayer.removeFromSuperlayer()
            }
        }
    }
    
    func seekToTime(_ time : TimeInterval) {
        if annotations == nil {
            return
        }
        if let sublayers = self.layer.sublayers {
            for (i, overlayLayer) in sublayers.enumerated() {
                
                let annotation = annotations![i]
                let beginTime = annotation.time
                let endTime = beginTime + GlobalConstants.Durations.annotationDuration
                
                overlayLayer.speed = 0.0
                if time < beginTime {
                    overlayLayer.timeOffset = 0.0
                } else if time > endTime {
                    overlayLayer.timeOffset = 1.0
                } else {
                    overlayLayer.timeOffset = 0.0
                    let percentageInAnimation = (endTime - time) / (endTime - beginTime)
                    overlayLayer.timeOffset = percentageInAnimation + CACurrentMediaTime()
                }
                
            }
        }

    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
