//
//  ShareHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 17/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import Foundation
import UIKit

class ShareSlackHelper : NSObject, UIDocumentInteractionControllerDelegate {
    
    let documentationInteractionController = UIDocumentInteractionController()
    
    func sendFromSlack(_ urlToSave : URL?, viewController : UIViewController) {
        //if let filePath =  NSBundle.mainBundle().pathForResource("test", ofType: "mov") {
        //let urlToSav = NSURL(fileURLWithPath: filePath)
        
        if let url = urlToSave {
            documentationInteractionController.url = url
            documentationInteractionController.uti = "com.apple.quicktime-movie"
            documentationInteractionController.delegate = self
            documentationInteractionController.presentOpenInMenu(from: CGRect(x: 0, y: 0, width: 0, height: 0), in: viewController.view, animated: true)
        }
        //}
    }
    
    
    func canOpenWhatsapp() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: "slack://app")!)
    }
}

