//
//  ImageObject.swift
//  Explainme
//
//  Created by Mehdi Kitane on 03/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import AVFoundation

extension CaptureObject {
    func takePicture(_ completionHandler: @escaping (_ image: UIImage) -> Void ){
        
        var videoConnection : AVCaptureConnection?
        for connection : AVCaptureConnection  in outputImage!.connections  {
            for port : AVCaptureInput.Port in connection.inputPorts {
                if port.mediaType == AVMediaType.video {
                    videoConnection = connection
                    break
                }
            }
            if (videoConnection != nil) {
                break
            }
        }
        
        
        func convertToImageOrientation(_ interfaceOrientation : UIInterfaceOrientation) -> UIImage.Orientation {
            switch interfaceOrientation {
            case .portrait:
                return UIImage.Orientation.right
            case .landscapeRight:
                return UIImage.Orientation.up
            case .landscapeLeft:
                return UIImage.Orientation.down
            case .portraitUpsideDown:
                return UIImage.Orientation.left
            default:
                return UIImage.Orientation.right
            }
        }
        func captureCompletionHandler(_ imageDataSampleBuffer : CMSampleBuffer?, error : Error?) -> Void {
            if let imageDataSampleBuffer = imageDataSampleBuffer {
                let orientation = convertToImageOrientation(UIApplication.shared.statusBarOrientation)
                
                let imageData : Data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)!
                
                let dataProvider = CGDataProvider(data: imageData as CFData)
                let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: orientation)
                
                //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                //let image : UIImage = UIImage(data: imageData, scale: 1.0)!
                completionHandler(image)
            }
        }
        
        CaptureObject.cameraQueue.async { () -> Void in
            self.outputImage!.captureStillImageAsynchronously(from: videoConnection!,completionHandler: captureCompletionHandler)
        }
    }
}
