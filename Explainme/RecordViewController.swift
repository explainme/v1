//
//  RecordViewController.swift
//  Explainme
//
//  Created by Mehdi Kitane on 10/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary
import QuartzCore
import MBProgressHUD


class RecordViewController: UIViewController, AnnotationViewDelegate, CaptureObjectProtocol, AudioCaptureProtocol {

    var outputFilePath: URL?
    
    @IBOutlet var globalView: UIView!
    @IBOutlet weak var safeAreaOverride: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var statusBar: UILabel!
    @IBOutlet weak var pauseButton: ProgressButton!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var fullscreenButton: UIButton!
    @IBOutlet weak var realPauseButton: UIButton!
    
    var savedImage : UIImage? //SavedImage if coming from ExplainViewController
    
    
    var videoCapture : CaptureObject?
    var audioCapture : AudioCapture?
    var annotationView : AnnotationView!
    var movieFileOutput : URL?
    var soundDisplayView : SoundDisplayView = SoundDisplayView()
    
    //var timer : NSTimer?
    var displayLink : CADisplayLink?
    
    var firstLayout = true
    var lastOrientation : UIInterfaceOrientation = UIInterfaceOrientation.unknown
    var photoComingFromLibrary = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastOrientation = RotationHandler.interfaceOrientation()
        RotationHandler.rotateInterfaceTo([pauseButton, realPauseButton, fullscreenButton], currentOrientation: lastOrientation, animate: false)

        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        cameraView.clipsToBounds = true

        annotationView = AnnotationView()
        annotationView.delegate = self
        annotationView.colorForAnnotations = GlobalConstants.Colors.blueForLayer
        soundDisplayView.backgroundColor = UIColor.clear
        soundDisplayView.totalLength = GlobalConstants.RecordingTimes.pictureRecordingTime

        
        // Do any additional setup after loading the view.
        if GlobalConstants.mediaChoice == .video {
            globalView.backgroundColor = GlobalConstants.Colors.pink
            statusBar.backgroundColor = GlobalConstants.Colors.pink
            annotationView.colorForAnnotations = GlobalConstants.Colors.pinkForLayer
            soundDisplayView.totalLength = GlobalConstants.RecordingTimes.videoRecordingTime
            pauseButton.setImage(UIImage(named: "stop_btn_red"), for: UIControl.State())
        }
        
        if GlobalConstants.mediaChoice == .video {
            self.videoCapture?.delegate = self
            self.videoCapture?.startRunning()
            self.videoCapture?.startRecording()
        }
        if GlobalConstants.mediaChoice == .photo {
            audioCapture = AudioCapture()
            self.audioCapture?.shouldWriteToFile = true
            audioCapture?.audioDelegate = self
            audioCapture?.startAudio()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if firstLayout {
            firstLayout = false
            
            self.videoCapture?.append(inView: self.cameraView, atlayerIndex: 0)
            if (GlobalConstants.mediaChoice == .photo) {
                if var savedImage = savedImage {

                    var newImage = rotateImage(savedImage.cgImage, orientation: savedImage.imageOrientation)
                    if photoComingFromLibrary {
                        if savedImage.size.width > savedImage.size.height {
                            savedImage = UIImage(cgImage: rotateImage(savedImage.cgImage, orientation: UIImage.Orientation.right)!)
                            newImage = savedImage.cgImage
                        }
                    }

                    cameraView.layer.contents = newImage
                    cameraView.layer.contentsGravity = GlobalConstants.Sizes.videoPreviewSize
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.displayLink = CADisplayLink(target: self, selector: #selector(RecordViewController.updateInterface))
        self.displayLink?.add(to: RunLoop.main, forMode: RunLoop.Mode.default)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //timer?.invalidate()
        displayLink?.invalidate()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        soundDisplayView.frame = CGRect(x: 0, y: 0, width: cameraView.bounds.width, height: cameraView.bounds.height - bottomView.bounds.height)
        cameraView.addSubview(soundDisplayView)
        
        annotationView.frame = cameraView.bounds
        cameraView.addSubview(annotationView)
    }

    @IBAction func clickedFullScreenButton(_ sender: AnyObject) {
        switchFullScreen()
    }
    
    func switchFullScreen() {
        if statusBar.isHidden == false {
            globalView.backgroundColor = .black
            safeAreaOverride.backgroundColor = .black

            statusBar.isHidden = true
            menuButton.isHidden = true
            bottomView.isHidden = true
            soundDisplayView.frame = CGRect(x: 0, y: 0, width: cameraView.bounds.width, height: cameraView.bounds.height)
            fullscreenButton.setImage(UIImage(named: "btn_fullscreen_close"), for: UIControl.State())
        } else {
            if GlobalConstants.mediaChoice == .video {
                globalView.backgroundColor = GlobalConstants.Colors.pink
            } else {
                globalView.backgroundColor = GlobalConstants.Colors.blue
            }
            safeAreaOverride.backgroundColor = .white

            statusBar.isHidden = false
            menuButton.isHidden = false
            bottomView.isHidden = false
            soundDisplayView.frame = CGRect(x: 0, y: 0, width: cameraView.bounds.width, height: cameraView.bounds.height - bottomView.bounds.height)
            fullscreenButton.setImage(UIImage(named: "btn_fullscreen"), for: UIControl.State())
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    @objc func updateInterface() {
        var maxTime : Double = -1  //60secs
        let currentTime = getTimeForAnnotation()
        
        
        if GlobalConstants.mediaChoice == .photo {
            soundDisplayView.updateWithAud(audioCapture!, currentTime: currentTime)
            maxTime = GlobalConstants.RecordingTimes.pictureRecordingTime
        }
        if GlobalConstants.mediaChoice == .video {
            soundDisplayView.updateWithVid(videoCapture!, currentTime: currentTime)
            maxTime = GlobalConstants.RecordingTimes.videoRecordingTime
        }
        
        statusBar.text = "\(timeStringForTimeInterval(currentTime)) of \(timeStringForTimeInterval(maxTime))"
        pauseButton.progression = CGFloat(currentTime)/CGFloat(maxTime)
        pauseButton.setNeedsDisplay()
    }
    func getTimeForAnnotation() -> CFTimeInterval {
        if GlobalConstants.mediaChoice == .photo {
            return (audioCapture?.currentTime())!
        }
        if GlobalConstants.mediaChoice == .video {
            return (videoCapture?.currentTime())!
        }
        return -1
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        //captureObjet.willRotate
    }
    
    override var shouldAutorotate : Bool {
        //return captureObject.shouldAutorotate()
        return true
    }

    // MARK: Actions
    @IBAction func toggleMovieRecord(_ sender: AnyObject) {
        /*
        let window = UIApplication.sharedApplication().windows.last
        let hud = MBProgressHUD.showHUDAddedTo(window, animated: true)
        hud.labelText = "Exporting video"        
        
        func completion(mediaChoice : MediaChoice) {
            let color = annotationView.colorForAnnotations
            let exporter = Exporter()
            if mediaChoice == .Photo {
                let avAsset = AVAsset(URL: self.movieFileOutput!)
                let image = self.savedImage!
                exporter.exportImage(avAsset, image: image, annotations: self.annotationView.annotations, annotationColor: color, exportQuality: GlobalConstants.exportQuality, completion: { (session) -> Void in
                    self.movieFileOutput = session.outputURL
                    hud.hide(true)
                    self.revealViewController().rearViewController.performSegueWithIdentifier("fromRecordViewToPreviewView", sender: self)
                })
            }
            
            if mediaChoice == .Video {
                let avAsset = AVAsset(URL: self.movieFileOutput!)
                exporter.exportVideo(avAsset, annotations: self.annotationView.annotations, annotationColor: color, exportQuality: GlobalConstants.exportQuality, completion: { (session) -> Void in
                    self.movieFileOutput = session.outputURL
                    hud.hide(true)
                    self.revealViewController().rearViewController.performSegueWithIdentifier("fromRecordViewToPreviewView", sender: self)
                })
            }
            
        }
        */
        if GlobalConstants.mediaChoice == .video {
            self.videoCapture?.stopRecording()
        }
        
        if GlobalConstants.mediaChoice == .photo {
            self.audioCapture?.stopAudio()
        }
        
        

    }
    
    @IBAction func changeCamera(_ sender: AnyObject) {
        print("change camera")
        /*self.cameraButton.enabled = false
        self.recordButton.enabled = false
        self.snapButton.enabled = false*/
    }
    
    @IBAction func focusAndExposeTap(_ gestureRecognizer: UIGestureRecognizer) {
        print("focusAndExposeTap")
    }
    
    // MARK: CaptureObjectProtocol
    func didFinishVideoRecordingToOutputFileAtURL(_ outputFileURL: URL!) {
        self.movieFileOutput = outputFileURL
        
        do {
            let asset = AVURLAsset(url: outputFileURL)
            let generateImg = AVAssetImageGenerator(asset: asset)
            let refImg = try generateImg.copyCGImage(at: CMTime.zero, actualTime: nil)
            self.savedImage = UIImage(cgImage: refImg)
            /*if let img = self.savedImage {
             let newImage = rotateImage(img.CGImage, orientation: img.imageOrientation)
             if let newImage = newImage {
             self.savedImage = UIImage(CGImage: newImage)
             }
             }*/
        } catch {
            
        }
        //completion(GlobalConstants.mediaChoice)
        DispatchQueue.main.async(execute: { () -> Void in
            self.revealViewController().rearViewController.performSegue(withIdentifier: "fromRecordViewToPreviewView", sender: self)
        })

    }
    func didFinishAudioRecordingToOutputFileAtURL(_ outputFileURL: URL!) {
        self.movieFileOutput = outputFileURL
        //completion(GlobalConstants.mediaChoice)
        DispatchQueue.main.async(execute: { () -> Void in
            self.revealViewController().rearViewController.performSegue(withIdentifier: "fromRecordViewToPreviewView", sender: self)
        })
    }

}
