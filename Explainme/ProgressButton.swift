//
//  ProgressButton.swift
//  Explainme
//
//  Created by Mehdi Kitane on 28/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import UIKit

class ProgressButton: UIButton {
    var progressLayer : ProgressButtonLayer?
    var progression : CGFloat = 0.0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        progressLayer = ProgressButtonLayer(frame: self.bounds)
        if let progressLayer = progressLayer {
            progressLayer.backgroundColor = UIColor.clear
            progressLayer.isUserInteractionEnabled = false
            self.addSubview(progressLayer)
        }
    }
    override func setNeedsDisplay() {
        super.setNeedsDisplay()
        progressLayer?.setNeedsDisplay()
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
