//
//  CameraViewController.swift
//  Explainme
//
//  Created by Mehdi Kitane on 25/11/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import CoreMedia

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var globalView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var cameraViewToEnableSideBar: UIView!
    @IBOutlet weak var videoCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var photoCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var photoLabel: UIButton!
    @IBOutlet weak var videoLabel: UIButton!
    @IBOutlet weak var statusBar: UILabel!
    
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var thumbnailButton: UIButton!
    
    @IBOutlet weak var capturedImage:UIImageView!
    
    @IBOutlet weak var menuButton: UIButton!
    
    var imageCapture : CaptureObject?
    var savedImage : UIImage? = nil
    var photoComingFromLibrary = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ExporterSingleton.sharedExporter.cancelExport()
        RotationHandler.deviceDidRotate([cameraBtn, thumbnailButton], animate: false)
        NotificationCenter.default.addObserver(self, selector: #selector(CameraViewController.deviceDidRotate), name: UIDevice.orientationDidChangeNotification, object: nil)

        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.cameraViewToEnableSideBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            //Configuring the revealViewController
            self.revealViewController().toggleAnimationType = SWRevealToggleAnimationType.spring
            self.revealViewController().frontViewShadowOpacity = 0.1
            self.revealViewController().frontViewShadowRadius = 5
            self.revealViewController().extendsPointInsideHit = true
        }
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(CameraViewController.swipeToRight(_:)))
        swipeRight.direction = .right
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(CameraViewController.swipeToLeft(_:)))
        swipeLeft.direction = .left
        cameraView.addGestureRecognizer(swipeRight)
        cameraView.addGestureRecognizer(swipeLeft)
        
        
        thumbnailButton.layer.cornerRadius = 4
        thumbnailButton.clipsToBounds = true
        
        
        GlobalConstants.mediaChoice = .photo
        self.globalView.backgroundColor = GlobalConstants.Colors.blue

        if((UserDefaults.standard.object(forKey: GlobalConstants.Texts.firstConnexion) as? Bool) != nil) {
            CaptureObject.checkDeviceAuthorizationStatus { (grantedVideo, firstTimeVideo) in
                AudioCapture.checkDeviceAuthorizationStatus({ (grantedAudio, firstTimeAudio) in
                    if !grantedVideo {
                        let message = "Express yourself using Explainᴹᴱ. Allow access to your camera and microphone to start using the Explainᴹᴱ app."
                        let alert = UIAlertController(title:"No Camera", message:message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"Change settings", style:UIAlertAction.Style.default, handler:{ action in
                            UIApplication.shared.openURL(URL(string:"prefs:root=Privacy&path=CAMERA")!)
                            
                        }))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated:true, completion:nil)
                        })
                    } else if !grantedAudio {
                        let message = "Express yourself using Explainᴹᴱ. Allow access to your camera and microphone to start using the Explainᴹᴱ app."
                        let alert = UIAlertController(title:"No Microphone", message:message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"Change settings", style:UIAlertAction.Style.default, handler:{ action in
                            UIApplication.shared.openURL(URL(string:"prefs:root=Privacy&path=MICROPHONE")!)
                        }))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated:true, completion:nil)
                        })
                    }
                    
                    if grantedAudio && grantedVideo {
                        CaptureObject.cameraQueue.async { () -> Void in
                            print("Will init CaptureObjet 1")
                            self.imageCapture = CaptureObject()
                            print("Will end CaptureObjet 1")
                            if firstTimeVideo {
                                self.doWhenViewWillAppearVideo()
                            }
                        }
                    }
                })
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.imageCapture?.append(inView: self.cameraView, atlayerIndex: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.revealViewController() != nil {
            if((UserDefaults.standard.object(forKey: GlobalConstants.Texts.firstConnexion) as? Bool) == nil) {
                self.revealViewController().rearViewController.performSegue(withIdentifier: "toTuto", sender: nil)
                return
            }
        }
        fetchLastPhoto(thumbnailButton)
        doWhenViewWillAppearVideo()
    }
    
    func doWhenViewWillAppearVideo() {
        CaptureObject.cameraQueue.async { () -> Void in
            print("Will init CaptureObjet 2")
            self.imageCapture?.willRotateToInterfaceOrientation(UIApplication.shared.statusBarOrientation, duration: 0)
            self.imageCapture?.startRunning()
            DispatchQueue.main.async(execute: {
                self.imageCapture?.append(inView: self.cameraView, atlayerIndex: 0)
                print("Will end init CaptureObjet 2")
            })
        }
    }
    

    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        self.imageCapture?.willRotateToInterfaceOrientation(toInterfaceOrientation, duration: duration)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.imageCapture?.didRotateToInterfaceOrientation(fromInterfaceOrientation, withView: self.cameraView)
    }
    
    @objc func deviceDidRotate() {
        RotationHandler.deviceDidRotate([cameraBtn, thumbnailButton])
    }
    
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func takePicture(_ sender: AnyObject) {
        if (GlobalConstants.mediaChoice == .photo) {
            imageCapture?.takePicture({ (image) -> Void in
                self.savedImage = image
                self.revealViewController().rearViewController.performSegue(withIdentifier: "fromCameraViewToExplainView", sender: self)
            })
        }
        else {
            self.revealViewController().rearViewController.performSegue(withIdentifier: "fromCameraViewToRecordView", sender: self)
        }
    }
    
    // MARK: - Gesture Methods
    // Swipe to switch from Video To Image and from Image to Video
    func changeToVideo(_ animated : Bool = true) {
        GlobalConstants.mediaChoice = .video
        
        self.videoCenterConstraint.isActive = true
        self.photoCenterConstraint.isActive = false
        self.view.needsUpdateConstraints()
        
        self.globalView.backgroundColor = GlobalConstants.Colors.pink
        self.statusBar.backgroundColor = GlobalConstants.Colors.pink
        self.statusBar.text = GlobalConstants.Texts.vidText
        self.cameraBtn.setImage(UIImage(named: "explain_btn_red.png"), for: UIControl.State())
        
        func completion() {
            self.videoLabel.titleLabel!.font = UIFont(name: "Lato-Bold", size:17.0)
            self.photoLabel.titleLabel!.font = UIFont(name: "Lato-Regular", size: 14.0)
            
            self.videoLabel.setTitleColor(UIColor.white, for: UIControl.State())
            self.photoLabel.setTitleColor(GlobalConstants.Colors.gray, for: UIControl.State())
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) -> Void in
                completion()
            }) 
        } else {
            completion()
            self.view.layoutIfNeeded()
        }
    }
    
    func changeToPhoto(_ animated : Bool = true) {
        GlobalConstants.mediaChoice = .photo
        
        self.videoCenterConstraint.isActive = false
        self.photoCenterConstraint.isActive = true
        self.view.needsUpdateConstraints()
        
        self.globalView.backgroundColor = GlobalConstants.Colors.blue
        self.statusBar.backgroundColor = GlobalConstants.Colors.blue
        self.statusBar.text = GlobalConstants.Texts.picText
        self.cameraBtn.setImage(UIImage(named: "photo_btn_blue.png"), for: UIControl.State())
        
        func completion() {
            self.videoLabel.titleLabel!.font = UIFont(name: "Lato-Regular", size: 14.0)
            self.photoLabel.titleLabel!.font = UIFont(name: "Lato-Bold", size:17.0)
            
            self.photoLabel.setTitleColor(UIColor.white, for: UIControl.State())
            self.videoLabel.setTitleColor(GlobalConstants.Colors.gray, for: UIControl.State())
        }
        if animated {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) -> Void in
                completion()
            }) 
        } else {
            completion()
            self.view.layoutIfNeeded()
        }
        
    }
    @objc func swipeToRight(_ gestureRecognizer: UISwipeGestureRecognizer) {
        //Video is the main one
        changeToVideo()
        //changeToVideo()
    }
    @objc func swipeToLeft(_ gestureRecognizer: UISwipeGestureRecognizer?) {
        //Photo is the main one
        changeToPhoto()
        //changeToPhoto()
    }
    @IBAction func clickedVideo(_ sender: AnyObject) {
        changeToVideo()
        //changeToVideo()
    }
    @IBAction func clickedPhoto(_ sender: AnyObject) {
        changeToPhoto()
        //changeToPhoto()
    }
    
    
    // MARK: - Thumbnail Methods
    @IBAction func clickedThumbnailButton(_ sender: AnyObject) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true) { () -> Void in
            GlobalConstants.mediaChoice = .photo
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.savedImage = image
            self.photoComingFromLibrary = true
            self.revealViewController().rearViewController.performSegue(withIdentifier: "fromCameraViewToExplainView", sender: self)
        }

    }
    
    // get the most recent photo and display it as thumbnail
    func fetchLastPhoto(_ toImageView : UIButton) {

        PHPhotoLibrary.requestAuthorization { (status : PHAuthorizationStatus) -> Void in
            if status == PHAuthorizationStatus.denied || status == PHAuthorizationStatus.restricted {
                return
            }

            let fetchOptions: PHFetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            let fetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
            
            if let lastAsset = fetchResult.lastObject as? PHAsset {

                let retinaScale = UIScreen.main.scale;
                let retinaSquare = CGSize(width: toImageView.bounds.size.width*retinaScale, height: toImageView.bounds.size.height*retinaScale);

                let options = PHImageRequestOptions()
                options.resizeMode = PHImageRequestOptionsResizeMode.exact

                PHImageManager.default().requestImage(for: lastAsset, targetSize: retinaSquare, contentMode: PHImageContentMode.aspectFill, options: options, resultHandler: { (result, info) -> Void in

                    DispatchQueue.main.async {
                        toImageView.setImage(result, for: UIControl.State())
                    }

                })
                
            }
            
        }
    }
    
}

