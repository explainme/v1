//
//  AnnotationViewDelegate.swift
//  TestVid
//
//  Created by Mehdi Kitane on 01/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//


@objc protocol AnnotationViewDelegate {
    func getTimeForAnnotation() -> CFTimeInterval
    
    @objc optional func didDrawAPoint()
    @objc optional func didDrawACircle()
    @objc optional func didDrawAnArrow()
}
