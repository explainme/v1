//
//  CaptureObjectExtension.swift
//  Explainme
//
//  Created by Mehdi Kitane on 03/03/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import AVFoundation

extension CaptureObject {
    static func checkDeviceAuthorizationStatus(_ completion : @escaping (_ granted : Bool, _ firstTime: Bool) -> Void ){
        let mediaType:String = AVMediaType.video.rawValue;
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType(rawValue: mediaType))
        if(authStatus == .authorized) {
            completion(true, false)
        } else if(authStatus == .denied){
            // denied
            completion(false, false)
        } else if(authStatus == .restricted){
            // restricted, normally won't happen
            completion(false, false)
        } else if(authStatus == .notDetermined){
            // not determined?!
            AVCaptureDevice.requestAccess(for: AVMediaType(rawValue: mediaType), completionHandler: { (granted: Bool) in
                //self.deviceAuthorized = granted;
                completion(granted, true)
            })
        } else {
            // impossible, unknown authorization status
            completion(false, false)
        }
    }
    
    func disableAutoFocus() {
        let currentDevice : AVCaptureDevice! = input?.device
        do {
            try currentDevice.lockForConfiguration()
            currentDevice.focusMode = AVCaptureDevice.FocusMode.locked
            currentDevice.unlockForConfiguration()
        } catch {
            
        }
    }
    func enableAutoFocus() {
        let currentDevice : AVCaptureDevice! = input?.device
        do {
            try currentDevice.lockForConfiguration()
            currentDevice.focusMode = AVCaptureDevice.FocusMode.autoFocus
            currentDevice.unlockForConfiguration()
        } catch {
            
        }
    }
    func focusAtPoint(_ touchPoint : CGPoint) {
        //Should do this before : touchPoint = gestureRecognizer.locationInView(gestureRecognizer.view)
        let convertedPoint : CGPoint = previewLayer.captureDevicePointConverted(fromLayerPoint: touchPoint)
        
        let currentDevice : AVCaptureDevice! = input?.device
        do {
            try currentDevice.lockForConfiguration()
        } catch {
            
        }
        
        if (currentDevice.isFocusPointOfInterestSupported && currentDevice.isFocusModeSupported(AVCaptureDevice.FocusMode.autoFocus)) {
            currentDevice.focusPointOfInterest = convertedPoint
            currentDevice.focusMode = AVCaptureDevice.FocusMode.autoFocus
        }
        
        if currentDevice.isExposurePointOfInterestSupported && currentDevice.isExposureModeSupported(AVCaptureDevice.ExposureMode.autoExpose) {
            
            currentDevice.exposurePointOfInterest = convertedPoint
            currentDevice.exposureMode = AVCaptureDevice.ExposureMode.autoExpose
            
        }
        //currentDevice.subjectAreaChangeMonitoringEnabled = true
        currentDevice.unlockForConfiguration()
    }
    
    func turnOnFlash() {
        let device : AVCaptureDevice! = input?.device
        if( device.hasTorch ) {
            do {
                try device.lockForConfiguration()
                device.torchMode = AVCaptureDevice.TorchMode.on
                device.unlockForConfiguration()
                
            } catch {
                
            }
        }
    }
    
    func turnOffFlash() {
        let device : AVCaptureDevice! = input?.device
        if( device.hasTorch ) {
            do {
                try device.lockForConfiguration()
                device.torchMode = AVCaptureDevice.TorchMode.off
                device.unlockForConfiguration()
            } catch {
                
            }
        }
    }
    
    func flashTurnedOn() -> Bool {
        return input?.device.torchMode == AVCaptureDevice.TorchMode.on
    }
    
    
    func switchCamera() {
        //Change camera source
        
        //Indicate that some changes will be made to the session
        session.beginConfiguration()
        
        
        //Remove existing input
        let currentCameraInput : AVCaptureInput = session.inputs[0] as! AVCaptureInput
        session.removeInput(currentCameraInput)
        
        
        // Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
        func cameraWithPosition(_ position : AVCaptureDevice.Position) -> AVCaptureDevice? {
            let devices = AVCaptureDevice.devices(for: AVMediaType.video)
            
            for device : AVCaptureDevice in devices as! Array<AVCaptureDevice> {
                if device.position == position {
                    return device
                }
            }
            return nil
        }
        
        //Get new input
        let newCamera : AVCaptureDevice?
        if (currentCameraInput as! AVCaptureDeviceInput).device.position == AVCaptureDevice.Position.back {
            newCamera = cameraWithPosition(AVCaptureDevice.Position.front)
        } else {
            newCamera = cameraWithPosition(AVCaptureDevice.Position.back)
        }
        
        //Add input to session
        //Input
        let newInput : AVCaptureDeviceInput?
        do {
            try newInput = AVCaptureDeviceInput(device: newCamera!)
        } catch {
            newInput = input
        }
        
        if session.canAddInput(newInput!)  {
            //AddNotifications?
            session.addInput(newInput!)
            input = newInput
        }
        
        //Commit all the configuration changes at once
        session.commitConfiguration()
    }
    
    func isBackCamera() -> Bool {
        return (session.inputs[0] as? AVCaptureDeviceInput)?.device.position == AVCaptureDevice.Position.back
    }

}
