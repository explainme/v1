//
//  ShareHelper.swift
//  Explainme
//
//  Created by Mehdi Kitane on 17/12/15.
//  Copyright © 2015 Mehdi Kitane. All rights reserved.
//

import Foundation
import UIKit

class ShareWhatsappHelper : NSObject, UIDocumentInteractionControllerDelegate {
    
    let documentationInteractionController = UIDocumentInteractionController()
    
    func sendFromWhatsapp(_ urlToSave : URL?, viewController : UIViewController) {
        //if let filePath =  NSBundle.mainBundle().pathForResource("test", ofType: "mov") {
            //let urlToSav = NSURL(fileURLWithPath: filePath)

            if let url = urlToSave, let _ = urlToSave?.pathExtension {
                //wam
                /*let fileManager = NSFileManager.defaultManager()
                var newURL = NSURL(string: url.URLString.stringByReplacingOccurrencesOfString(pathExtension, withString: "wam"))!

                do {
                    try fileManager.copyItemAtURL(url, toURL: newURL)
                } catch {
                    newURL = url
                }*/
                
                documentationInteractionController.url = url
                documentationInteractionController.uti = "net.whatsapp.movie"
                documentationInteractionController.delegate = self
                documentationInteractionController.presentOpenInMenu(from: CGRect.zero, in: viewController.view, animated: true)
            }
        //}
    }
    
    
    func canOpenWhatsapp() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: "whatsapp://app")!)
    }
}

