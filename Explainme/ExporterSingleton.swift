//
//  ExporterSingleton.swift
//  Explainme
//
//  Created by Mehdi Kitane on 01/09/2016.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//

import Foundation
import AVFoundation

class ExporterSingleton {
    static let sharedExporter = ExporterSingleton()
    
    var exporter : Exporter
    var movieFileOutput : URL?
    
    fileprivate init() {
        exporter = Exporter()
    }
    
    func exportImage(_ avAsset : AVAsset, image : UIImage, annotations : [Annotation], annotationColor : UIColor, exportQuality : String,
                     lastOrientation: UIInterfaceOrientation, sizeVideo: CGSize, shouldCropVideoUsingImage: Bool, completion: @escaping (_ session: AVAssetExportSession) -> Void ) {
        
        movieFileOutput = nil
        exporter.exportImage(avAsset, image: image, annotations: annotations, annotationColor: annotationColor, exportQuality: exportQuality, lastOrientation: lastOrientation, sizeVideo: sizeVideo, shouldCropVideoUsingImage: shouldCropVideoUsingImage) { (session) in

            
            self.movieFileOutput = session.outputURL
            NotificationCenter.default.post(name: Notification.Name(rawValue: "finishedExporting"), object: nil)
            completion(session)
        }
    }
    
    func exportVideo(_ avAsset : AVAsset, annotations : [Annotation], annotationColor : UIColor, exportQuality : String, lastOrientation: UIInterfaceOrientation, completion: @escaping (_ session: AVAssetExportSession) -> Void ) {
        
        movieFileOutput = nil
        exporter.exportVideo(avAsset, annotations: annotations, annotationColor: annotationColor, exportQuality: exportQuality, lastOrientation: lastOrientation) { (session) in
            
            self.movieFileOutput = session.outputURL
            NotificationCenter.default.post(name: Notification.Name(rawValue: "finishedExporting"), object: nil)
            completion(session)
        }
    }
    
    func cancelExport() {
        if let realExporter = exporter.exporter {
            movieFileOutput = nil
            realExporter.cancelExport()
        }
    }

}
