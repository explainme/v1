//
//  UIGestureRecognizer.swift
//  TestVid
//
//  Created by Mehdi Kitane on 29/02/16.
//  Copyright © 2016 Mehdi Kitane. All rights reserved.
//


import UIKit
import UIKit.UIGestureRecognizerSubclass

class MKGestureRecognizer: UIGestureRecognizer {
    var touchedPoints = [CGPoint]() // point history

    var path = CGMutablePath() // running CGPath - helps with drawing

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        if touches.count != 1 {
            state = .failed
        }
        state = .began

        
        //Drawing may need to moove
        //let window = view?.window
        if let loc = touches.first?.location(in: view) {
            path.move(to: loc) // start the path
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        
        // 1
        if state == .failed {
            return
        }
        
        // 2
        //let window = view?.window
        if let loc = touches.first?.location(in: view) {
            touchedPoints.append(loc) // 3
            path.addLine(to: loc)
            state = .changed // 4
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        
        //state = isCircle ? .Ended : .Failed
        state = .ended
    }
    
    override func reset() {
        super.reset()
        touchedPoints.removeAll(keepingCapacity: true)
        path = CGMutablePath()
        state = .possible
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesCancelled(touches, with: event)
        state = .cancelled // forward the cancel state
    }
}
